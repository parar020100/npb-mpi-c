SHELL=/bin/sh
CLASS=A
LANG=M
SUBTYPE=
VERSION=
SFILE=config/suite.def


ifeq "$(LANG)" "F"
SWITCH_FILE = .lang_f.used
SWITCH_OTHER_1 = .lang_c.used
SWITCH_OTHER_2 = .lang_m.used
MIXED_LANG_MACRO_ACTION = define

else ifeq "$(LANG)" "C"
SWITCH_FILE = .lang_c.used
SWITCH_OTHER_1 = .lang_f.used
SWITCH_OTHER_2 = .lang_m.used
MIXED_LANG_MACRO_ACTION = undef

else ifeq "$(LANG)" "M"
SWITCH_FILE = .lang_m.used
SWITCH_OTHER_1 = .lang_f.used
SWITCH_OTHER_2 = .lang_c.used
MIXED_LANG_MACRO_ACTION = define
endif

ifdef ENABLE_OMP
SWITCH_OMP_FILE = .omp.used
SWITCH_OMP_OTHER_FILE = .no_omp.used
else
SWITCH_OMP_FILE = .no_omp.used
SWITCH_OMP_OTHER_FILE = .omp.used
endif

LANG_MACRO = "$(MIXED_LANG_MACRO_ACTION) FORTRAN_INTERACTION"

.NOPARALLEL:

default: SP

#default: header
#	@ sys/print_instructions

SP: sp
sp: switch params header
	cd SP; $(MAKE) CLASS=$(CLASS) LANG=$(LANG)


ifneq ("$(wildcard $(SWITCH_OTHER_1))","")
should_switch = 1
endif
ifneq ("$(wildcard $(SWITCH_OTHER_2))","")
should_switch = 1
endif
ifeq ("$(wildcard SP/lang_mode.h)","")
should_switch = 1
endif
ifeq ("$(wildcard $(SWITCH_FILE))","")
should_switch = 1
endif

ifneq ("$(wildcard $(SWITCH_OMP_OTHER_FILE))","")
should_switch_omp = 1
endif
ifeq ("$(wildcard $(SWITCH_OMP_FILE))","")
should_switch_omp = 1
endif

switch:
ifdef should_switch
	rm -f $(SWITCH_OTHER_1) $(SWITCH_OTHER_2) SP/lang_mode.h SP/*.o
	echo "#$(LANG_MACRO)" > SP/lang_mode.h
	touch $(SWITCH_FILE)
endif
ifdef should_switch_omp
	rm -f $(SWITCH_OMP_OTHER_FILE) SP/*.o
	touch $(SWITCH_OMP_FILE)
endif

params:
	@cd ./sys; ${MAKE} all
	@cd SP; ../sys/setparams sp $(CLASS)


# Awk script courtesy cmg@cray.com, modified by Haoqiang Jin
suite:
	@ awk -f sys/suite.awk SMAKE=$(MAKE) $(SFILE) | $(SHELL)


# It would be nice to make clean in each subdirectory (the targets
# are defined) but on a really clean system this will won't work
# because those makefiles need config/make.def
clean:
	- rm -f core *~ */core */*~
	- rm -f */*.o */*.mod */*.obj */*.exe */npbparams.h */lang_mode.h */npbparams_c.h
	- rm -f MPI_dummy/test MPI_dummy/libmpi.a
	- rm -f sys/setparams sys/makesuite sys/setparams.h
	- rm -f btio.*.out* .*.used

binclean: clean
	- rm -f bin/sp.*

veryclean: clean
	- rm -f config/make.def config/suite.def 
	- rm -f bin/sp.* bin/lu.* bin/mg.* bin/ft.* bin/bt.* bin/is.* 
	- rm -f bin/ep.* bin/cg.* bin/dt.*

header:
	@ sys/print_header

test:
	./run.sh

cleantest: binclean
	./run.sh

compile:
	./run.sh -c

run:
	./run.sh -r

