#include "sp_data_c.h"

void add_(void)
{
    for (int c = 1; c <= ncells; c++)
        for (int m = 1; m <= 5; m++)
            for (int k = START(c,3); k <= CELL_SIZE(c,3) - END(c,3) - 1; k++)
                for (int j = START(c,2); j <= CELL_SIZE(c,2) - END(c,2) - 1; j++)
                    for (int i = START(c,1); i <= CELL_SIZE(c,1) - END(c,1) - 1; i++)
                        U(c,m,k,j,i) += RHS(c,m,k,j,i);

}