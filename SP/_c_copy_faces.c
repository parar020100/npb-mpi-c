#include "sp_data_c.h"
#include <mpi.h>
#include "mpinpb_c.h"

//---------------------------------------------------------------------
// this function copies the face values of a variable defined on a set
// of cells to the overlap locations of the adjacent sets of cells.
// Because a set of cells interfaces in each direction with exactly one
// other set, we only need to fill six different buffers. We could try to
// overlap communication with computatioan, by computing
// some internal values while communicating boundary values, but this
// adds so much overhead that it's not clearly useful.
//---------------------------------------------------------------------
void copy_faces_(void)
{
	int i, j, k, c, m, p0, p1, p2, p3, p4, p5;
	int b_size[6], ss[6], sr[6];

	MPI_Request requests[12];
	MPI_Status statuses[12];

	//---------------------------------------------------------------------
	//      exit immediately if there are no faces to be copied
	//---------------------------------------------------------------------
	if (no_nodes == 1)
	{
		compute_rhs_();
		return;
	}

	ss[0] = start_send_east;
	ss[1] = start_send_west;
	ss[2] = start_send_north;
	ss[3] = start_send_south;
	ss[4] = start_send_top;
	ss[5] = start_send_bottom;

	sr[0] = start_recv_east;
	sr[1] = start_recv_west;
	sr[2] = start_recv_north;
	sr[3] = start_recv_south;
	sr[4] = start_recv_top;
	sr[5] = start_recv_bottom;

	b_size[0] = east_size;
	b_size[1] = west_size;
	b_size[2] = north_size;
	b_size[3] = south_size;
	b_size[4] = top_size;
	b_size[5] = bottom_size;

	//---------------------------------------------------------------------
	// because the difference stencil for the diagonalized scheme is
	// orthogonal, we do not have to perform the staged copying of faces,
	// but can send all face information simultaneously to the neighboring
	// cells in all directions
	//---------------------------------------------------------------------

	if (timeron)
		timer_start(t_bpack);

	p0 = 0;
	p1 = 0;
	p2 = 0;
	p3 = 0;
	p4 = 0;
	p5 = 0;


	for (c = 1; c <= ncells; c++)
		for (m = 1; m <= 5; m++)
		{
			//------------------------------------------------------------
			//   fill the buffer to be sent to eastern neighbors (i-dir)
			//------------------------------------------------------------

			if (CELL_COORD(c,1) != ncells)
				for (k = 0; k <= CELL_SIZE(c,3) - 1; k++)
					for (j = 0; j <= CELL_SIZE(c,2) - 1; j++)
						for (i = CELL_SIZE(c,1) - 2; i <= CELL_SIZE(c,1) - 1; i++)
						{
							OUT_BUFFER(ss[0]+p0) = U(c,m,k,j,i);
							p0++;
						}

			//------------------------------------------------------------
			//   fill the buffer to be sent to western neighbors
			//------------------------------------------------------------

			if (CELL_COORD(c,1) != 1)
				for (k = 0; k <= CELL_SIZE(c,3) - 1; k++)
					for (j = 0; j <= CELL_SIZE(c,2) - 1; j++)
						for (i = 0; i <= 1; i++)
						{
							OUT_BUFFER(ss[1]+p1) = U(c,m,k,j,i);
							p1++;
						}

			//------------------------------------------------------------
			//   fill the buffer to be sent to northern neighbors (j_dir)
			//------------------------------------------------------------

			if (CELL_COORD(c,2) != ncells)
				for (k = 0; k <= CELL_SIZE(c,3) - 1; k++)
					for (j = CELL_SIZE(c,2) - 2; j <= CELL_SIZE(c,2) - 1; j++)
						for (i = 0; i <= CELL_SIZE(c,1) - 1; i++)
						{
							OUT_BUFFER(ss[2]+p2) = U(c,m,k,j,i);
							p2++;
						}

			//------------------------------------------------------------
			//   fill the buffer to be sent to southern neighbors
			//------------------------------------------------------------

			if (CELL_COORD(c,2) != 1)
				for (k = 0; k <= CELL_SIZE(c,3) - 1; k++)
					for (j = 0; j <= 1; j++)
						for (i = 0; i <= CELL_SIZE(c,1) - 1; i++)
						{
							OUT_BUFFER(ss[3]+p3) = U(c,m,k,j,i);
							p3++;
						}

			//------------------------------------------------------------
			//   fill the buffer to be sent to top neighbors (k-dir)
			//------------------------------------------------------------

			if (CELL_COORD(c,3) != ncells)
				for (k = CELL_SIZE(c,3) - 2; k <= CELL_SIZE(c,3) - 1; k++)
					for (j = 0; j <= CELL_SIZE(c,2) - 1; j++)
						for (i = 0; i <= CELL_SIZE(c,1) - 1; i++)
						{
							OUT_BUFFER(ss[4]+p4) = U(c,m,k,j,i);
							p4++;
						}

			//------------------------------------------------------------
			//   fill the buffer to be sent to bottom neighbors
			//------------------------------------------------------------

			if (CELL_COORD(c,3) != 1)
				for (k = 0; k <= 1; k++)
					for (j = 0; j <= CELL_SIZE(c,2) - 1; j++)
						for (i = 0; i <= CELL_SIZE(c,1) - 1; i++)
						{
							OUT_BUFFER(ss[5]+p5) = U(c,m,k,j,i);
							p5++;
						}

		}

	if (timeron)
	{
		timer_stop(t_bpack);
		timer_start(t_exch);
	}

	MPI_Irecv(&IN_BUFFER(sr[0]), b_size[0], dp_type, SUCCESSOR(1),
			  WEST, comm_rhs, &requests[0]);
	MPI_Irecv(&IN_BUFFER(sr[1]), b_size[1], dp_type, PREDECESSOR(1),
			  EAST, comm_rhs, &requests[1]);
	MPI_Irecv(&IN_BUFFER(sr[2]), b_size[2], dp_type, SUCCESSOR(2),
			  SOUTH, comm_rhs, &requests[2]);
	MPI_Irecv(&IN_BUFFER(sr[3]), b_size[3], dp_type, PREDECESSOR(2),
			  NORTH, comm_rhs, &requests[3]);
	MPI_Irecv(&IN_BUFFER(sr[4]), b_size[4], dp_type, SUCCESSOR(3),
			  BOTTOM, comm_rhs, &requests[4]);
	MPI_Irecv(&IN_BUFFER(sr[5]), b_size[5], dp_type, PREDECESSOR(3),
			  TOP, comm_rhs, &requests[5]);


	MPI_Isend(&OUT_BUFFER(ss[0]), b_size[0], dp_type, SUCCESSOR(1),
			  EAST, comm_rhs, &requests[6]);
	MPI_Isend(&OUT_BUFFER(ss[1]), b_size[1], dp_type, PREDECESSOR(1),
			  WEST, comm_rhs, &requests[7]);
	MPI_Isend(&OUT_BUFFER(ss[2]), b_size[2], dp_type, SUCCESSOR(2),
			  NORTH, comm_rhs, &requests[8]);
	MPI_Isend(&OUT_BUFFER(ss[3]), b_size[3], dp_type, PREDECESSOR(2),
			  SOUTH, comm_rhs, &requests[9]);
	MPI_Isend(&OUT_BUFFER(ss[4]), b_size[4], dp_type, SUCCESSOR(3),
			  TOP, comm_rhs, &requests[10]);
	MPI_Isend(&OUT_BUFFER(ss[5]), b_size[5], dp_type, PREDECESSOR(3),
			  BOTTOM, comm_rhs, &requests[11]);

	MPI_Waitall(12, requests, statuses);

	if (timeron)
	{
		timer_stop(t_exch);

		//---------------------------------------------------------------------
		// unpack the data that has just been received;
		//---------------------------------------------------------------------
		timer_start(t_bpack);
	}

	p0 = 0;
	p1 = 0;
	p2 = 0;
	p3 = 0;
	p4 = 0;
	p5 = 0;

	for (c = 1; c <= ncells; c++)
		for (m = 1; m <= 5; m++)
		{

			if (CELL_COORD(c,1) != 1)
				for (k = 0; k <= CELL_SIZE(c,3) - 1; k++)
					for (j = 0; j <= CELL_SIZE(c,2) - 1; j++)
						for (i = -2; i <= -1; i++)
						{
							U(c,m,k,j,i) = IN_BUFFER(sr[1]+p0);
							p0++;
						}

			if (CELL_COORD(c,1) != ncells)
				for (k = 0; k <= CELL_SIZE(c,3) - 1; k++)
					for (j = 0; j <= CELL_SIZE(c,2) - 1; j++)
						for (i = CELL_SIZE(c,1); i <= CELL_SIZE(c,1) + 1; i++)
						{
							U(c,m,k,j,i) = IN_BUFFER(sr[0]+p1);
							p1++;
						}

			if (CELL_COORD(c,2) != 1)
				for (k = 0; k <= CELL_SIZE(c,3) - 1; k++)
					for (j = -2; j <= -1; j++)
						for (i = 0; i <= CELL_SIZE(c,1) - 1; i++)
						{
							U(c,m,k,j,i) = IN_BUFFER(sr[3]+p2);
							p2++;
						}

			if (CELL_COORD(c,2) != ncells)
				for (k = 0; k <= CELL_SIZE(c,3) - 1; k++)
					for (j = CELL_SIZE(c,2); j <= CELL_SIZE(c,2) + 1; j++)
						for (i = 0; i <= CELL_SIZE(c,1) - 1; i++)
						{
							U(c,m,k,j,i) = IN_BUFFER(sr[2]+p3);
							p3++;
						}

			if (CELL_COORD(c,3) != 1)
				for (k = -2; k <= -1; k++)
					for (j = 0; j <= CELL_SIZE(c,2) - 1; j++)
						for (i = 0; i <= CELL_SIZE(c,1) - 1; i++)
						{
							U(c,m,k,j,i) = IN_BUFFER(sr[5]+p4);
							p4++;
						}

			if (CELL_COORD(c,3) != ncells)
				for (k = CELL_SIZE(c,3); k <= CELL_SIZE(c,3) + 1; k++)
					for (j = 0; j <= CELL_SIZE(c,2) - 1; j++)
						for (i = 0; i <= CELL_SIZE(c,1) - 1; i++)
						{
							U(c,m,k,j,i) = IN_BUFFER(sr[4]+p5);
							p5++;
						}

		}

	if (timeron)
		timer_stop(t_bpack);

	//---------------------------------------------------------------------
	// now that we have all the data, compute the rhs
	//---------------------------------------------------------------------

	compute_rhs_();
	return;
}




