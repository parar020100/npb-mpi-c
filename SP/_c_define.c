#include "sp_data_c.h"


void compute_buffer_size_(int *p_dim)
{
	int dim = *p_dim;

	int face_size;

	if (ncells == 1)
		return;

	west_size = 0;
	east_size = 0;

	for(int c = 1; c <= ncells; c++)
	{
		face_size = CELL_SIZE(c,2) * CELL_SIZE(c,3) * dim * 2;
		if (CELL_COORD(c,1) != 1)
			west_size += face_size;
		if (CELL_COORD(c,1) != ncells)
			east_size += face_size;
	}

	north_size = 0;
	south_size = 0;

	for(int c = 1; c <= ncells; c++)
	{
		face_size = CELL_SIZE(c,1) * CELL_SIZE(c,3) * dim * 2;
		if (CELL_COORD(c,2) != 1)
			south_size += face_size;
		if (CELL_COORD(c,2) != ncells)
			north_size += face_size;
	}

	top_size = 0;
	bottom_size = 0;

	for(int c = 1; c <= ncells; c++)
	{
		face_size = CELL_SIZE(c,1) * CELL_SIZE(c,2) * dim * 2;
		if (CELL_COORD(c,3) != 1)
			bottom_size += face_size;
		if (CELL_COORD(c,3) != ncells)
			top_size += face_size;
	}

	start_send_west   = 1;
	start_send_east   = start_send_west   + west_size;
	start_send_south  = start_send_east   + east_size;
	start_send_north  = start_send_south  + south_size;
	start_send_bottom = start_send_north  + north_size;
	start_send_top    = start_send_bottom + bottom_size;
	start_recv_west   = 1;
	start_recv_east   = start_recv_west   + west_size;
	start_recv_south  = start_recv_east   + east_size;
	start_recv_north  = start_recv_south  + south_size;
	start_recv_bottom = start_recv_north  + north_size;
	start_recv_top    = start_recv_bottom + bottom_size;

}