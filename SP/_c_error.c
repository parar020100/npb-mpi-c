#include "sp_data_c.h"
#include <mpi.h>
#include <math.h>
#include "mpinpb_c.h"

//---------------------------------------------------------------------
// this function computes the norm of the difference between the
// computed solution and the exact solution
//---------------------------------------------------------------------

void error_norm_(double rms[5])
{
	int c, i, j, k, m, ii, jj, kk, d;
	double xi, eta, zeta, u_exact[5], rms_work[5], add;

	for (m = 0; m < 5; m++)
		rms_work[m] = 0.0;

	for (c = 1; c <= ncells; c++)
	{
		kk = 0;
		for (k = CELL_LOW(c,3); k <= CELL_HIGH(c,3); k++)
		{
			zeta = ((double) k) * dnzm1;
			jj = 0;

			for (j = CELL_LOW(c,2); j <= CELL_HIGH(c,2); j++)
			{
				eta = ((double) j) * dnym1;
				ii = 0;
				for (i = CELL_LOW(c,1); i <= CELL_HIGH(c,1); i++)
				{
					xi = ((double) i) * dnxm1;
					exact_solution_(&xi, &eta, &zeta, u_exact);

					for (m = 1; m <= 5; m++)
					{
						add = U(c,m,kk,jj,ii) - u_exact[m - 1];
						rms_work[m - 1] += add * add;
					}
					ii++;
				}
				jj++;
			}
			kk++;
		}
	}

	MPI_Allreduce(rms_work, rms, 5, dp_type, MPI_SUM, comm_setup);

	for (m = 1; m <= 5; m++)
	{
		for (d = 1; d <= 3; d++)
			rms[m - 1] = rms[m - 1] / ((double) GRID_POINTS(d) - 2);
		rms[m - 1] = sqrt(rms[m - 1]);
	}

	return;
}


void rhs_norm_(double rms[5])
{
	int c, i, j, k, d, m;
	double rms_work[5], add;

	for (m = 1; m <= 5; m++)
		rms_work[m - 1] = 0.0;

	for (c = 1; c <= ncells; c++)
		for (k = START(c,3); k <= CELL_SIZE(c,3) - END(c,3) - 1; k++)
			for (j = START(c,2); j <= CELL_SIZE(c,2) - END(c,2) - 1; j++)
				for (i = START(c,1); i <= CELL_SIZE(c,1) - END(c,1) - 1; i++)
					for (m = 1; m <= 5; m++)
					{
						add = RHS(c,m,k,j,i);
						rms_work[m - 1] += add * add;
					}

	MPI_Allreduce(rms_work, rms, 5, dp_type, MPI_SUM, comm_setup);

	for (m = 1; m <= 5; m++)
	{
		for (d = 1; d <= 3; d++)
			rms[m - 1] = rms[m - 1] / ((double) GRID_POINTS(d) - 2);
		rms[m - 1] = sqrt(rms[m - 1]);
	}

	return;
}

