#include "sp_data_c.h"
#include "mpinpb_c.h"

//---------------------------------------------------------------------
//---------------------------------------------------------------------

void exact_rhs_(void)
{
	//---------------------------------------------------------------------
	// compute the right hand side based on exact solution
	//---------------------------------------------------------------------

	double dtemp[5], xi, eta, zeta, dtpp;
	int c, m, i, j, k, ip1, im1, jp1, jm1, km1, kp1;

	//---------------------------------------------------------------------
	// loop over all cells owned by this node
	//---------------------------------------------------------------------

	for (c = 1; c <= ncells; c++)
	{
		//---------------------------------------------------------------------
		//         initialize
		//---------------------------------------------------------------------
		for (m = 1; m <= 5; m++)
			#pragma omp parallel for schedule(static)
			for(k = 0; k <= CELL_SIZE(c,3) - 1; k++)
				for(j = 0; j <= CELL_SIZE(c,2) - 1; j++)
					for(i = 0; i <= CELL_SIZE(c,1) - 1; i++)
						FORCING(c,m,k,j,i) = 0.0;

		//---------------------------------------------------------------------
		// xi-direction flux differences
		//---------------------------------------------------------------------

		for (k = START(c,3); k <= CELL_SIZE(c,3) - END(c,3) - 1; k++)
		{
			zeta = ((double) (k + CELL_LOW(c,3))) * dnzm1;
			for (j = START(c,2); j <= CELL_SIZE(c,2) - END(c,2) - 1; j++)
			{
				eta = ((double) (j + CELL_LOW(c,2))) * dnym1;

				for (i = -2 * (1 - START(c,1)); i <= CELL_SIZE(c,1) + 1 - 2 * END(c,1); i++)
				{

					xi = ((double) (i + CELL_LOW(c,1))) * dnxm1;
					exact_solution_(&xi, &eta, &zeta, dtemp);

					for (m = 1; m <= 5; m++)
						UE(m,i) = dtemp[m - 1];

					dtpp = 1.0 / dtemp[0];

					for(m = 2; m <= 5; m++)
						BUF(m,i) = dtpp * dtemp[m - 1];

					CUF(i) = BUF(2,i) * BUF(2,i);
					BUF(1,i) = CUF(i) + BUF(3,i) * BUF(3,i) + BUF(4,i) * BUF(4,i);
					Q(i) = 0.5 *
						(BUF(2,i) * UE(2,i) + BUF(3,i) * UE(3,i) + BUF(4,i) * UE(4,i));

				}

				#pragma omp parallel for default(shared) private(i,im1,ip1) schedule(static)
				for (i = START(c,1); i <= CELL_SIZE(c,1) - END(c,1) - 1; i++)
				{
					im1 = i - 1;
					ip1 = i + 1;

					FORCING(c,1,k,j,i) -= tx2 * (UE(2,ip1) - UE(2,im1));
					FORCING(c,1,k,j,i) += dx1tx1 * (UE(1,ip1) - 2.0 * UE(1,i) + UE(1,im1));

					FORCING(c,2,k,j,i) -= tx2 * (UE(2,ip1) * BUF(2,ip1) + c2 * (UE(5,ip1) - Q(ip1)));
					FORCING(c,2,k,j,i) += tx2 * (UE(2,im1) * BUF(2,im1) + c2 * (UE(5,im1) - Q(im1)));
					FORCING(c,2,k,j,i) += xxcon1 * (BUF(2,ip1) - 2.0 * BUF(2,i) + BUF(2,im1));
					FORCING(c,2,k,j,i) += dx2tx1 * ( UE(2,ip1) - 2.0* UE(2,i) + UE(2,im1));

					FORCING(c,3,k,j,i) -= tx2 * (UE(3,ip1) * BUF(2,ip1) - UE(3,im1) * BUF(2,im1));
					FORCING(c,3,k,j,i) += xxcon2 * (BUF(3,ip1) - 2.0 * BUF(3,i) + BUF(3,im1));
					FORCING(c,3,k,j,i) += dx3tx1 * (UE(3,ip1) - 2.0 * UE(3,i) + UE(3,im1));

					FORCING(c,4,k,j,i) -= tx2 * (UE(4,ip1) * BUF(2,ip1) - UE(4,im1) * BUF(2,im1));
					FORCING(c,4,k,j,i) += xxcon2 * (BUF(4,ip1) - 2.0 * BUF(4,i) + BUF(4,im1));
					FORCING(c,4,k,j,i) += dx4tx1 * (UE(4,ip1) - 2.0* UE(4,i) + UE(4,im1));

					FORCING(c,5,k,j,i) -= tx2 * BUF(2,ip1) * (c1 * UE(5,ip1) - c2 * Q(ip1));
					FORCING(c,5,k,j,i) += tx2 * BUF(2,im1) * (c1 * UE(5,im1) - c2 * Q(im1));
					FORCING(c,5,k,j,i) += 0.5 * xxcon3 * (BUF(1,ip1) - 2.0 * BUF(1,i) + BUF(1,im1));
					FORCING(c,5,k,j,i) += xxcon4 * (CUF(ip1) - 2.0 * CUF(i) + CUF(im1));
					FORCING(c,5,k,j,i) += xxcon5 * (BUF(5,ip1) - 2.0 * BUF(5,i) + BUF(5,im1));
					FORCING(c,5,k,j,i) += dx5tx1 * ( UE(5,ip1) - 2.0* UE(5,i)+ UE(5,im1));
				}

				//---------------------------------------------------------------------
				// Fourth-order dissipation
				//---------------------------------------------------------------------
				if (START(c,1) > 0)
					#pragma omp parallel for default(shared) private(i,m) schedule(static)
					for (m = 1; m <= 5; m++)
					{
						i = 1;
						FORCING(c,m,k,j,i) -= dssp * (5.0 * UE(m,i) - 4.0 * UE(m,i+1) + UE(m,i+2));
						i = 2;
						FORCING(c,m,k,j,i) -= dssp *
							(-4.0 * UE(m,i-1) + 6.0 * UE(m,i) - 4.0 * UE(m,i+1) + UE(m,i+2));
					}

				for (m = 1; m <= 5; m++)
					#pragma omp parallel for default(shared) private(i) schedule(static)
					for (i = START(c,1) * 3; i <= CELL_SIZE(c,1) - 3 * END(c,1) - 1; i++)
						FORCING(c,m,k,j,i) -= dssp *
							(UE(m,i-2) - 4.0 * UE(m,i-1)
							 + 6.0 * UE(m,i) - 4.0 * UE(m,i+1) + UE(m,i+2));

				if (END(c,1) > 0)
					#pragma omp parallel for default(shared) private(i,m) schedule(static)
					for (m = 1; m <= 5; m++)
					{
						i = CELL_SIZE(c,1) - 3;
						FORCING(c,m,k,j,i) -= dssp *
							(UE(m,i-2) - 4.0 * UE(m,i-1) + 6.0 * UE(m,i) - 4.0 * UE(m,i+1));

						i = CELL_SIZE(c,1) - 2;
						FORCING(c,m,k,j,i) -= dssp * (UE(m,i-2) - 4.0 * UE(m,i-1) + 5.0 * UE(m,i));
					}

			}
		}


		//---------------------------------------------------------------------
		//  eta-direction flux differences
		//---------------------------------------------------------------------
		for (k = START(c,3); k <= CELL_SIZE(c,3) - END(c,3) - 1; k++)
		{
			zeta = ((double) (k + CELL_LOW(c,3))) * dnzm1;
			for (i = START(c,1); i <= CELL_SIZE(c,1) - END(c,1) - 1; i++)
			{
				xi = ((double) (i + CELL_LOW(c,1))) * dnxm1;

				for (j = -2 * (1 - START(c,2)); j <= CELL_SIZE(c,2) + 1 - 2 * END(c,2); j++)
				{
					eta = ((double) (j + CELL_LOW(c,2))) * dnym1;

					exact_solution_(&xi, &eta, &zeta, dtemp);
					for (m = 1; m <= 5; m++)
						UE(m,j) = dtemp[m - 1];

					dtpp = 1.0 / dtemp[0];

					for (m = 2; m <= 5; m++)
						BUF(m,j) = dtpp * dtemp[m - 1];

					CUF(j) = BUF(3,j) * BUF(3,j);
					BUF(1,j) = CUF(j) + BUF(2,j) * BUF(2,j) + BUF(4,j) * BUF(4,j);
					Q(j) = 0.5 * (BUF(2,j) * UE(2,j) + BUF(3,j) * UE(3,j) + BUF(4,j) * UE(4,j));
				}

				#pragma omp parallel for default(shared) private(j,jm1,jp1) schedule(static)
				for (j = START(c,2); j <= CELL_SIZE(c,2) - END(c,2) - 1; j++)
				{
					jm1 = j - 1;
					jp1 = j + 1;

					FORCING(c,1,k,j,i) -= ty2 * (UE(3,jp1) - UE(3,jm1));
					FORCING(c,1,k,j,i) += dy1ty1 * (UE(1,jp1) - 2.0 * UE(1,j) + UE(1,jm1));

					FORCING(c,2,k,j,i) -= ty2 * (UE(2,jp1) * BUF(3,jp1) - UE(2,jm1) * BUF(3,jm1));
					FORCING(c,2,k,j,i) += yycon2 * (BUF(2,jp1) - 2.0 * BUF(2,j) + BUF(2,jm1));
					FORCING(c,2,k,j,i) += dy2ty1 * ( UE(2,jp1) - 2.0* UE(2,j)+ UE(2,jm1));

					FORCING(c,3,k,j,i) -= ty2 * (UE(3,jp1) * BUF(3,jp1) + c2 * (UE(5,jp1) - Q(jp1)) );
					FORCING(c,3,k,j,i) += ty2 * (UE(3,jm1) * BUF(3,jm1) + c2 * (UE(5,jm1) - Q(jm1)) );
					FORCING(c,3,k,j,i) += yycon1 * (BUF(3,jp1) - 2.0 * BUF(3,j) + BUF(3,jm1));
					FORCING(c,3,k,j,i) += dy3ty1 * (UE(3,jp1) - 2.0 * UE(3,j) + UE(3,jm1));

					FORCING(c,4,k,j,i) -= ty2 * (UE(4,jp1) * BUF(3,jp1) - UE(4,jm1) * BUF(3,jm1));
					FORCING(c,4,k,j,i) += yycon2 * (BUF(4,jp1) - 2.0 * BUF(4,j) + BUF(4,jm1));
					FORCING(c,4,k,j,i) += dy4ty1 * ( UE(4,jp1) - 2.0 * UE(4,j)+ UE(4,jm1));

					FORCING(c,5,k,j,i) -= ty2 * BUF(3,jp1) * (c1 * UE(5,jp1) - c2 * Q(jp1));
					FORCING(c,5,k,j,i) += ty2 * BUF(3,jm1) * (c1 * UE(5,jm1) - c2 * Q(jm1));
					FORCING(c,5,k,j,i) += 0.5 * yycon3 * (BUF(1,jp1) - 2.0 * BUF(1,j) + BUF(1,jm1));
					FORCING(c,5,k,j,i) += yycon4 * (CUF(jp1) - 2.0 * CUF(j) + CUF(jm1));
					FORCING(c,5,k,j,i) += yycon5 * (BUF(5,jp1) - 2.0 * BUF(5,j) + BUF(5,jm1));
					FORCING(c,5,k,j,i) += dy5ty1 * (UE(5,jp1) - 2.0 * UE(5,j) + UE(5,jm1));

				}

				//---------------------------------------------------------------------
				// Fourth-order dissipation
				//---------------------------------------------------------------------
				if (START(c,2) > 0)
					#pragma omp parallel for default(shared) private(j,m) schedule(static)
					for (m = 1; m <= 5; m++)
					{
						j = 1;
						FORCING(c,m,k,j,i) -= dssp * (5.0 * UE(m,j) - 4.0 * UE(m,j+1) + UE(m,j+2));
						j = 2;
						FORCING(c,m,k,j,i) -= dssp *
							(-4.0 * UE(m,j-1) + 6.0 * UE(m,j) - 4.0 * UE(m,j+1) + UE(m,j+2));
					}

				for (m = 1; m <= 5; m++)
					#pragma omp parallel for default(shared) private(j) schedule(static)
					for (j = START(c,2) * 3; j <= CELL_SIZE(c,2) - 3 * END(c,2) - 1; j++)
						FORCING(c,m,k,j,i) -= dssp *
							(UE(m,j-2) - 4.0 * UE(m,j-1) + 6.0 * UE(m,j)
							 - 4.0 * UE(m,j+1) + UE(m,j+2));

				if (END(c,2) > 0)
					#pragma omp parallel for default(shared) private(j,m) schedule(static)
					for (m = 1; m <= 5; m++)
					{
						j = CELL_SIZE(c,2) - 3;
						FORCING(c,m,k,j,i) -= dssp *
							(UE(m,j-2) - 4.0 * UE(m,j-1) + 6.0 * UE(m,j) - 4.0 * UE(m,j+1));
						j = CELL_SIZE(c,2) - 2;
						FORCING(c,m,k,j,i) = FORCING(c,m,k,j,i) - dssp *
							(UE(m,j-2) - 4.0 * UE(m,j-1) + 5.0 * UE(m,j));
					}

			}
		}

		//---------------------------------------------------------------------
		// zeta-direction flux differences
		//---------------------------------------------------------------------
		for (j = START(c,2); j <= CELL_SIZE(c,2) - END(c,2) - 1; j++)
		{
			eta = ((double) (j + CELL_LOW(c,2))) * dnym1;
			for (i = START(c,1); i <= CELL_SIZE(c,1) - END(c,1) - 1; i++)
			{
				xi = ((double) (i + CELL_LOW(c,1))) * dnxm1;

				for (k = -2 * (1 - START(c,3)); k <= CELL_SIZE(c,3) + 1 - 2 * END(c,3); k++)
				{
					zeta = ((double) (k + CELL_LOW(c,3))) * dnzm1;

					exact_solution_(&xi, &eta, &zeta, dtemp);

					for (m = 1; m <= 5; m++)
						UE(m,k) = dtemp[m - 1];

					dtpp = 1.0 / dtemp[0];

					for (m = 2; m <= 5; m++)
						BUF(m,k) = dtpp * dtemp[m - 1];

					CUF(k) = BUF(4,k) * BUF(4,k);
					BUF(1,k) = CUF(k) + BUF(2,k) * BUF(2,k) + BUF(3,k) * BUF(3,k);
					Q(k) = 0.5 * (BUF(2,k) * UE(2,k) + BUF(3,k) * UE(3,k) + BUF(4,k) * UE(4,k));
				}

				#pragma omp parallel for default(shared) private(k,km1,kp1) schedule(static)
				for (k = START(c,3); k <= CELL_SIZE(c,3) - END(c,3) - 1; k++)
				{
					km1 = k - 1;
					kp1 = k + 1;

					FORCING(c,1,k,j,i) -= tz2 * (UE(4,kp1) - UE(4,km1));
					FORCING(c,1,k,j,i) += dz1tz1 * (UE(1,kp1) - 2.0 * UE(1,k) + UE(1,km1));

					FORCING(c,2,k,j,i) -= tz2 * (UE(2,kp1) * BUF(4,kp1) - UE(2,km1) * BUF(4,km1));
					FORCING(c,2,k,j,i) += zzcon2 * (BUF(2,kp1) - 2.0 * BUF(2,k) + BUF(2,km1));
					FORCING(c,2,k,j,i) += dz2tz1 * ( UE(2,kp1) - 2.0 * UE(2,k)+ UE(2,km1));

					FORCING(c,3,k,j,i) -= tz2 * (UE(3,kp1) * BUF(4,kp1) - UE(3,km1) * BUF(4,km1));
					FORCING(c,3,k,j,i) += zzcon2 * (BUF(3,kp1) - 2.0 * BUF(3,k) + BUF(3,km1));
					FORCING(c,3,k,j,i) += dz3tz1 * (UE(3,kp1) - 2.0 * UE(3,k) + UE(3,km1));

					FORCING(c,4,k,j,i) -= tz2 * (UE(4,kp1) * BUF(4,kp1) + c2 * (UE(5,kp1) - Q(kp1)));
					FORCING(c,4,k,j,i) += tz2 * (UE(4,km1) * BUF(4,km1) + c2 * (UE(5,km1) - Q(km1)));
					FORCING(c,4,k,j,i) += zzcon1 * (BUF(4,kp1) - 2.0 * BUF(4,k) + BUF(4,km1));
					FORCING(c,4,k,j,i) += dz4tz1 * ( UE(4,kp1) - 2.0 * UE(4,k) +UE(4,km1));

					FORCING(c,5,k,j,i) -= tz2 * BUF(4,kp1) * (c1 * UE(5,kp1) - c2 * Q(kp1));
					FORCING(c,5,k,j,i) += tz2 * BUF(4,km1) * (c1 * UE(5,km1) - c2 * Q(km1));
					FORCING(c,5,k,j,i) += 0.5 * zzcon3 * (BUF(1,kp1) - 2.0 * BUF(1,k) + BUF(1,km1));
					FORCING(c,5,k,j,i) += zzcon4 * (CUF(kp1) - 2.0 * CUF(k) + CUF(km1));
					FORCING(c,5,k,j,i) += zzcon5 * (BUF(5,kp1) - 2.0 * BUF(5,k) + BUF(5,km1));
					FORCING(c,5,k,j,i) += dz5tz1 * ( UE(5,kp1) - 2.0 * UE(5,k)+ UE(5,km1));
				}

				//---------------------------------------------------------------------
				// Fourth-order dissipation
				//---------------------------------------------------------------------
				if (START(c,3) > 0)
					#pragma omp parallel for default(shared) private(k,m) schedule(static)
					for (m = 1; m <= 5; m++)
					{
						k = 1;
						FORCING(c,m,k,j,i) -= dssp * (5.0 * UE(m,k) - 4.0 * UE(m,k+1) + UE(m,k+2));
						k = 2;
						FORCING(c,m,k,j,i) -= dssp *
							(-4.0 * UE(m,k-1) + 6.0 * UE(m,k) - 4.0 * UE(m,k+1) + UE(m,k+2));
					}

				for (m = 1; m <= 5; m++)
					#pragma omp parallel for default(shared) private(k) schedule(static)
					for (k = START(c,3) * 3; k <= CELL_SIZE(c,3) - 3 * END(c,3) - 1; k++)
						FORCING(c,m,k,j,i) -= dssp *
							(UE(m,k-2) - 4.0 * UE(m,k-1) + 6.0 * UE(m,k)
							 - 4.0 * UE(m,k+1) + UE(m,k+2));


				if (END(c,3) > 0)
					#pragma omp parallel for default(shared) private(k,m) schedule(static)
					for (m = 1; m <= 5; m++)
					{
						k = CELL_SIZE(c,3) - 3;
						FORCING(c,m,k,j,i) -= dssp *
							(UE(m,k-2) - 4.0 * UE(m,k-1) + 6.0 * UE(m,k) - 4.0 * UE(m,k+1));
						k = CELL_SIZE(c,3) - 2;
						FORCING(c,m,k,j,i) -= dssp * (UE(m,k-2) - 4.0 * UE(m,k-1) + 5.0 * UE(m,k));
					}

			}
		}
		//---------------------------------------------------------------------
		//   now change the sign of the forcing function,
		//---------------------------------------------------------------------
		for (m = 1; m <= 5; m++)
			#pragma omp parallel for default(shared) private(i,j,k) schedule(static)
			for (k = START(c,3); k <= CELL_SIZE(c,3) - END(c,3) - 1; k++)
				for (j = START(c,2); j <= CELL_SIZE(c,2) - END(c,2) - 1; j++)
					for (i = START(c,1); i <= CELL_SIZE(c,1) - END(c,1) - 1; i++)
						FORCING(c,m,k,j,i) *= -1.0;

		//---------------------------------------------------------------------
		//   cell loop
		//---------------------------------------------------------------------
	}


	return;
}



