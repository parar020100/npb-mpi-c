#include "sp_data_c.h"

// returns the exact solution at point xi, eta, zeta

void exact_solution_(double *p_xi, double *p_eta, double *p_zeta, double dtemp[5])
{
    double xi = *p_xi;
    double eta = *p_eta;
    double zeta = *p_zeta;

    #pragma omp parallel for default(shared) schedule(static)
    for (int m = 1; m <= 5; m++) {
        dtemp[m - 1] = CE(1,m) +
            xi  *(CE(2,m) + xi  *(CE(5,m) + xi  *(CE(8, m) + xi  *CE(11,m)))) +
            eta *(CE(3,m) + eta *(CE(6,m) + eta *(CE(9, m) + eta *CE(12,m)))) +
            zeta*(CE(4,m) + zeta*(CE(7,m) + zeta*(CE(10,m) + zeta*CE(13,m))));
    }
}

