#include "sp_data_c.h"

void initialize_(void)
{
	int c, i, j, k, m, ii, jj, kk, ix, iy, iz;
	double xi, eta, zeta, Pface[2][3][5], Pxi, Peta, Pzeta, temp[5];

	int k_from, k_to, j_from, j_to, i_from, i_to;
	double tmp;

	for (c = 1; c <= ncells; c++)
		#pragma omp parallel for default(shared) private(kk,jj,ii) schedule(static)
		for (kk = -1; kk <= IMAX; kk++)
			for (jj = -1; jj <= IMAX; jj++)
				for (ii = -1; ii <= IMAX; ii++)
				{
					U(c,1,kk,jj,ii) = 1.0;
					U(c,2,kk,jj,ii) = 0.0;
					U(c,3,kk,jj,ii) = 0.0;
					U(c,4,kk,jj,ii) = 0.0;
					U(c,5,kk,jj,ii) = 1.0;
				}

	for (c = 1; c <= ncells; c++)
	{
		kk = 0;

		k_from = CELL_LOW(c,3);
		k_to   = CELL_HIGH(c,3);

		j_from = CELL_LOW(c,2);
		j_to   = CELL_HIGH(c,2);

		i_from = CELL_LOW(c,1);
		i_to   = CELL_HIGH(c,1);

		for (k = k_from; k <= k_to; k++)
		{
			zeta = ((double) k) * dnzm1;
			jj = 0;

			for (j = j_from; j <= j_to; j++)
			{
				eta = ((double) j) * dnym1;
				ii = 0;

				for (i = i_from; i <= i_to; i++)
				{
					xi = ((double) i) * dnxm1;

					for(ix = 1; ix <= 2; ix++)
					{
						tmp = (ix - 1);
						exact_solution_(&tmp, &eta, &zeta, Pface[ix-1][0]);
					}

					for(iy = 1; iy <= 2; iy++)
					{
						tmp = (iy - 1);
						exact_solution_(&xi, &tmp, &zeta, Pface[iy-1][1]);
					}

					for(iz = 1; iz <= 2; iz++)
					{
						tmp = (iz - 1);
						exact_solution_(&xi, &eta, &tmp, Pface[iz-1][2]);
					}

					#pragma omp parallel for default(shared) private(m,Pxi,Peta,Pzeta) schedule(static)
					for(m = 1; m <= 5; m++)
					{
						Pxi   = xi   * Pface[1][0][m-1]
							+ (1.0 - xi)   * Pface[0][0][m-1];

						Peta  = eta  * Pface[1][1][m-1]
							+ (1.0 - eta)  * Pface[0][1][m-1];

						Pzeta = zeta * Pface[1][2][m-1]
							+ (1.0 - zeta) * Pface[0][2][m-1];

						U(c,m,kk,jj,ii) = Pxi + Peta + Pzeta
							- (Pxi * Peta + Pxi * Pzeta + Peta * Pzeta)
							+ Pxi * Peta * Pzeta;
					}
					ii++;
				}
				jj++;
			}
			kk++;
		}
	}

	// west face
	c = SLICE(1,1);

	k_from = CELL_LOW(c,3);
	k_to   = CELL_HIGH(c,3);

	j_from = CELL_LOW(c,2);
	j_to   = CELL_HIGH(c,2);

	ii = 0;
	xi = 0.0;
	kk = 0;
	for (k = k_from; k <= k_to; k++)
	{
		zeta = ((double) k) * dnzm1;
		jj = 0;

		for (j = j_from; j <= j_to; j++)
		{
			eta = ((double) j) * dnym1;
			exact_solution_(&xi, &eta, &zeta, temp);

			for(m = 1; m <= 5; m++)
				U(c,m,kk,jj,ii) = temp[m - 1];

			jj++;
		}
		kk++;
	}

	// east face
	c = SLICE(ncells,1);

	k_from = CELL_LOW(c,3);
	k_to   = CELL_HIGH(c,3);

	j_from = CELL_LOW(c,2);
	j_to   = CELL_HIGH(c,2);

	ii = CELL_SIZE(c,1) - 1;
	xi = 1.0;
	kk = 0;
	for (k = k_from; k <= k_to; k++)
	{
		zeta = ((double) k) * dnzm1;
		jj = 0;

		for (j = j_from; j <= j_to; j++)
		{
			eta = ((double) j) * dnym1;
			exact_solution_(&xi, &eta, &zeta, temp);

			for(m = 1; m <= 5; m++)
				U(c,m,kk,jj,ii) = temp[m - 1];

			jj++;
		}
		kk++;
	}

	// south face
	c = SLICE(1,2);

	k_from = CELL_LOW(c,3);
	k_to   = CELL_HIGH(c,3);

	i_from = CELL_LOW(c,1);
	i_to   = CELL_HIGH(c,1);

	jj = 0;
	eta = 0.0;
	kk = 0;
	for (k = k_from; k <= k_to; k++)
	{
		zeta = ((double) k) * dnzm1;
		ii = 0;

		for (i = i_from; i <= i_to; i++)
		{
			xi = ((double) i) * dnxm1;
			exact_solution_(&xi, &eta, &zeta, temp);

			for(m = 1; m <= 5; m++)
				U(c,m,kk,jj,ii) = temp[m - 1];

			ii++;
		}
		kk++;
	}

	// north face
	c = SLICE(ncells,2);

	k_from = CELL_LOW(c,3);
	k_to   = CELL_HIGH(c,3);

	i_from = CELL_LOW(c,1);
	i_to   = CELL_HIGH(c,1);

	jj = CELL_SIZE(c,2) - 1;
	eta = 1.0;
	kk = 0;

	for (k = k_from; k <= k_to; k++)
	{
		zeta = ((double) k) * dnzm1;
		ii = 0;

		for (i = i_from; i <= i_to; i++)
		{
			xi = ((double) i) * dnxm1;
			exact_solution_(&xi, &eta, &zeta, temp);

			for(m = 1; m <= 5; m++)
				U(c,m,kk,jj,ii) = temp[m - 1];

			ii++;
		}
		kk++;
	}

	// bottom face
	c = SLICE(1,3);

	j_from = CELL_LOW(c,2);
	j_to   = CELL_HIGH(c,2);

	i_from = CELL_LOW(c,1);
	i_to   = CELL_HIGH(c,1);

	kk = 0;
	zeta = 0.0;
	jj = 0;

	for (j = j_from; j <= j_to; j++)
	{
		eta = ((double) j) * dnym1;
		ii = 0;
		for (i = i_from; i <= i_to; i++)
		{
			xi = ((double) i) * dnxm1;
			exact_solution_(&xi, &eta, &zeta, temp);

			for(m = 1; m <= 5; m++)
				U(c,m,kk,jj,ii) = temp[m - 1];

			ii++;
		}
		jj++;
	}

	// top face
	c = SLICE(ncells,3);

	j_from = CELL_LOW(c,2);
	j_to   = CELL_HIGH(c,2);

	i_from = CELL_LOW(c,1);
	i_to   = CELL_HIGH(c,1);

	kk = CELL_SIZE(c,3) - 1;;
	zeta = 1.0;
	jj = 0;

	for (j = j_from; j <= j_to; j++)
	{
		eta = ((double) j) * dnym1;
		ii = 0;
		for (i = i_from; i <= i_to; i++)
		{
			xi = ((double) i) * dnxm1;
			exact_solution_(&xi, &eta, &zeta, temp);

			for(m = 1; m <= 5; m++)
				U(c,m,kk,jj,ii) = temp[m - 1];

			ii++;
		}
		jj++;
	}

	return;
}

void lhsinit_(void)
{
	int i, j, k, d, c, n;

	for (c = 1; c <= ncells; c++)
	{
		for (d = 1; d <= 3; d++)
		{
			if (CELL_COORD(c,d) == 1)
				START(c,d) = 1;
			else
				START(c,d) = 0;

			if (CELL_COORD(c,d) == ncells)
				END(c,d) = 1;
			else
				END(c,d) = 0;
		}

		int k_to   = CELL_SIZE(c,3) - 1;
		int j_to   = CELL_SIZE(c,2) - 1;
		int i_to   = CELL_SIZE(c,1) - 1;

		for(n = 1; n <= 15; n++)
			for(k = 0; k <= k_to; k++)
				for(j = 0; j <= j_to; j++)
					for(i = 0; i <= i_to; i++)
						LHS(c,n,k,j,i) = 0.0;

		for (n = 1; n <= 3; n++)
			for(k = 0; k <= k_to; k++)
				for(j = 0; j <= j_to; j++)
					for(i = 0; i <= i_to; i++)
						LHS(c,(5*n-2),k,j,i) = 1.0;


	}
}