#include "sp_data_c.h"

void lhsx_(int *p_c)
{
	//---------------------------------------------------------------------
	// This function computes the left hand side for the three x-factors
	//---------------------------------------------------------------------
	double ru1;
	int i, j, k, c;

	c = *p_c;

	//---------------------------------------------------------------------
	//      treat only cell c
	//---------------------------------------------------------------------

	//---------------------------------------------------------------------
	//      first fill the lhs for the u-eigenvalue
	//---------------------------------------------------------------------
	for (k = START(c,3); k <= CELL_SIZE(c,3) - END(c,3) - 1; k++)
		for (j = START(c,2); j <= CELL_SIZE(c,2) - END(c,2) - 1; j++)
		#pragma omp parallel default(shared) private(i,ru1)
		{
			#pragma omp for schedule(static)
			for(i = START(c,1) - 1; i <= CELL_SIZE(c,1) - END(c,1); i++)
			{
				ru1 = c3c4 * RHO_I(c,k,j,i);
				CV(i) = US(c,k,j,i);
				RHON(i) = max(max(dx2 + con43 * ru1, dx5 + c1c5 * ru1),
							  max(dxmax + ru1, dx1));
			}
			#pragma omp for schedule(static)
			for(i = START(c,1); i <= CELL_SIZE(c,1) - END(c,1) - 1; i++)
			{
				LHS(c,1,k,j,i) = 0;
				LHS(c,2,k,j,i) = - dttx2 * CV(i-1) - dttx1 * RHON(i-1);
				LHS(c,3,k,j,i) = 1.0 + c2dttx1 * RHON(i);
				LHS(c,4,k,j,i) =   dttx2 * CV(i+1) - dttx1 * RHON(i+1);
				LHS(c,5,k,j,i) = 0;
			}
		}

	//---------------------------------------------------------------------
	//      add fourth order dissipation
	//---------------------------------------------------------------------

	#pragma omp parallel default(shared) private(i,j,k)
	{
		if (START(c,1) > 0)
		{
			i = 1;
			#pragma omp for schedule(static)
			for (k = START(c,3); k <= CELL_SIZE(c,3) - END(c,3) - 1; k++)
				for (j = START(c,2); j <= CELL_SIZE(c,2) - END(c,2) - 1; j++)
				{
					LHS(c,3,k,j,i) += comz5;
					LHS(c,4,k,j,i) -= comz4;
					LHS(c,5,k,j,i) += comz1;

					LHS(c,2,k,j,i+1) -= comz4;
					LHS(c,3,k,j,i+1) += comz6;
					LHS(c,4,k,j,i+1) -= comz4;
					LHS(c,5,k,j,i+1) += comz1;
				}
		}

		#pragma omp for schedule(static)
		for (k = START(c,3); k <= CELL_SIZE(c,3) - END(c,3) - 1; k++)
			for (j = START(c,2); j <= CELL_SIZE(c,2) - END(c,2) - 1; j++)
				for (i = 3 * START(c,1); i <= CELL_SIZE(c,1) - 3 * END(c,1) - 1; i++)
				{
					LHS(c,1,k,j,i) += comz1;
					LHS(c,2,k,j,i) -= comz4;
					LHS(c,3,k,j,i) += comz6;
					LHS(c,4,k,j,i) -= comz4;
					LHS(c,5,k,j,i) += comz1;
				}

		if (END(c,1) > 0)
		{
			i = CELL_SIZE(c,1) - 3;
			#pragma omp for schedule(static)
			for (k = START(c,3); k <= CELL_SIZE(c,3) - END(c,3) - 1; k++)
				for (j = START(c,2); j <= CELL_SIZE(c,2) - END(c,2) - 1; j++)
				{
					LHS(c,1,k,j,i) += comz1;
					LHS(c,2,k,j,i) -= comz4;
					LHS(c,3,k,j,i) += comz6;
					LHS(c,4,k,j,i) -= comz4;

					LHS(c,1,k,j,i+1) += comz1;
					LHS(c,2,k,j,i+1) -= comz4;
					LHS(c,3,k,j,i+1) += comz5;
				}
		}

		//---------------------------------------------------------------------
		//      subsequently, fill the other factors (u+c), (u-c) by a4ing to
		//      the first
		//---------------------------------------------------------------------
		#pragma omp for schedule(static)
		for (k = START(c,3); k <= CELL_SIZE(c,3) - END(c,3) - 1; k++)
			for (j = START(c,2); j <= CELL_SIZE(c,2) - END(c,2) - 1; j++)
				for (i = START(c,1); i <= CELL_SIZE(c,1) - END(c,1) - 1; i++)
				{
					LHS(c,1+5,k,j,i)  = LHS(c,1,k,j,i);
					LHS(c,2+5,k,j,i)  = LHS(c,2,k,j,i) - dttx2 * SPEED(c,k,j,i-1);
					LHS(c,3+5,k,j,i)  = LHS(c,3,k,j,i);
					LHS(c,4+5,k,j,i)  = LHS(c,4,k,j,i) + dttx2 * SPEED(c,k,j,i+1);
					LHS(c,5+5,k,j,i)  = LHS(c,5,k,j,i);
					LHS(c,1+10,k,j,i) = LHS(c,1,k,j,i);
					LHS(c,2+10,k,j,i) = LHS(c,2,k,j,i) + dttx2 * SPEED(c,k,j,i-1);
					LHS(c,3+10,k,j,i) = LHS(c,3,k,j,i);
					LHS(c,4+10,k,j,i) = LHS(c,4,k,j,i) - dttx2 * SPEED(c,k,j,i+1);
					LHS(c,5+10,k,j,i) = LHS(c,5,k,j,i);
				}
	}

	return;
}