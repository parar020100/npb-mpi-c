#include "sp_data_c.h"

void lhsy_(int *p_c)
{
	//---------------------------------------------------------------------
	// This function computes the left hand side for the three y-factors
	//---------------------------------------------------------------------
	double ru1;
	int i, j, k, c;

	c = *p_c;

	//---------------------------------------------------------------------
	//      treat only cell c
	//---------------------------------------------------------------------

	//---------------------------------------------------------------------
	//      first fill the lhs for the u-eigenvalue
	//---------------------------------------------------------------------
	for (k = START(c,3); k <= CELL_SIZE(c,3) - END(c,3) - 1; k++)
		for (i = START(c,1); i <= CELL_SIZE(c,1) - END(c,1) - 1; i++)
		#pragma omp parallel default(shared) private(j,ru1)
		{
			#pragma omp for schedule(static)
			for(j = START(c,2) - 1; j <= CELL_SIZE(c,2) - END(c,2); j++)
			{
				ru1 = c3c4 * RHO_I(c,k,j,i);
				CV(j) = VS(c,k,j,i);
				RHOQ(j) = max(max(dy3 + con43 * ru1, dy5 + c1c5 * ru1),
							  max(dymax + ru1, dy1));
			}
			#pragma omp for schedule(static)
			for(j = START(c,2); j <= CELL_SIZE(c,2) - END(c,2) - 1; j++)
			{
				LHS(c,1,k,j,i) = 0;
				LHS(c,2,k,j,i) = - dtty2 * CV(j-1) - dtty1 * RHOQ(j-1);
				LHS(c,3,k,j,i) = 1.0 + c2dtty1 * RHOQ(j);
				LHS(c,4,k,j,i) =   dtty2 * CV(j+1) - dtty1 * RHOQ(j+1);
				LHS(c,5,k,j,i) = 0;
			}
		}

	//---------------------------------------------------------------------
	//      add fourth order dissipation
	//---------------------------------------------------------------------

	#pragma omp parallel default(shared) private(i,j,k)
	{
		if (START(c,2) > 0)
		{
			j = 1;
			#pragma omp for schedule(static)
			for (k = START(c,3); k <= CELL_SIZE(c,3) - END(c,3) - 1; k++)
				for (i = START(c,1); i <= CELL_SIZE(c,1) - END(c,1) - 1; i++)
				{
					LHS(c,3,k,j,i) += comz5;
					LHS(c,4,k,j,i) -= comz4;
					LHS(c,5,k,j,i) += comz1;

					LHS(c,2,k,j+1,i) -= comz4;
					LHS(c,3,k,j+1,i) += comz6;
					LHS(c,4,k,j+1,i) -= comz4;
					LHS(c,5,k,j+1,i) += comz1;
				}
		}

		#pragma omp for schedule(static)
		for (k = START(c,3); k <= CELL_SIZE(c,3) - END(c,3) - 1; k++)
			for (j = 3 * START(c,2); j <= CELL_SIZE(c,2) - 3 * END(c,2) - 1; j++)
				for (i = START(c,1); i <= CELL_SIZE(c,1) - END(c,1) - 1; i++)
				{
					LHS(c,1,k,j,i) += comz1;
					LHS(c,2,k,j,i) -= comz4;
					LHS(c,3,k,j,i) += comz6;
					LHS(c,4,k,j,i) -= comz4;
					LHS(c,5,k,j,i) += comz1;
				}

		if (END(c,2) > 0)
		{
			j = CELL_SIZE(c,2) - 3;
			#pragma omp for schedule(static)
			for (k = START(c,3); k <= CELL_SIZE(c,3) - END(c,3) - 1; k++)
				for (i = START(c,1); i <= CELL_SIZE(c,1) - END(c,1) - 1; i++)
				{
					LHS(c,1,k,j,i) += comz1;
					LHS(c,2,k,j,i) -= comz4;
					LHS(c,3,k,j,i) += comz6;
					LHS(c,4,k,j,i) -= comz4;

					LHS(c,1,k,j+1,i) += comz1;
					LHS(c,2,k,j+1,i) -= comz4;
					LHS(c,3,k,j+1,i) += comz5;
				}
		}

		//---------------------------------------------------------------------
		//      subsequently, do the other two factors
		//---------------------------------------------------------------------
		#pragma omp for schedule(static)
		for (k = START(c,3); k <= CELL_SIZE(c,3) - END(c,3) - 1; k++)
			for (j = START(c,2); j <= CELL_SIZE(c,2) - END(c,2) - 1; j++)
				for (i = START(c,1); i <= CELL_SIZE(c,1) - END(c,1) - 1; i++)
				{
					LHS(c,1+5,k,j,i)  = LHS(c,1,k,j,i);
					LHS(c,2+5,k,j,i)  = LHS(c,2,k,j,i) - dtty2 * SPEED(c,k,j-1,i);
					LHS(c,3+5,k,j,i)  = LHS(c,3,k,j,i);
					LHS(c,4+5,k,j,i)  = LHS(c,4,k,j,i) + dtty2 * SPEED(c,k,j+1,i);
					LHS(c,5+5,k,j,i)  = LHS(c,5,k,j,i);
					LHS(c,1+10,k,j,i) = LHS(c,1,k,j,i);
					LHS(c,2+10,k,j,i) = LHS(c,2,k,j,i) + dtty2 * SPEED(c,k,j-1,i);
					LHS(c,3+10,k,j,i) = LHS(c,3,k,j,i);
					LHS(c,4+10,k,j,i) = LHS(c,4,k,j,i) - dtty2 * SPEED(c,k,j+1,i);
					LHS(c,5+10,k,j,i) = LHS(c,5,k,j,i);
				}
	}

	return;
}