#include "sp_data_c.h"

void lhsz_(int *p_c)
{
	//---------------------------------------------------------------------
	// This function computes the left hand side for the three y-factors
	//---------------------------------------------------------------------
	double ru1;
	int i, j, k, c;

	c = *p_c;

	//---------------------------------------------------------------------
	//      treat only cell c
	//---------------------------------------------------------------------

	//---------------------------------------------------------------------
	//      first fill the lhs for the u-eigenvalue
	//---------------------------------------------------------------------
	for (j = START(c,2); j <= CELL_SIZE(c,2) - END(c,2) - 1; j++)
		for (i = START(c,1); i <= CELL_SIZE(c,1) - END(c,1) - 1; i++)
		#pragma omp parallel default(shared) private(k,ru1)
		{
			#pragma omp for schedule(static)
			for(k = START(c,3) - 1; k <= CELL_SIZE(c,3) - END(c,3); k++)
			{
				ru1 = c3c4 * RHO_I(c,k,j,i);
				CV(k) = WS(c,k,j,i);
				RHOS(k) = max(max(dz4 + con43 * ru1, dz5 + c1c5 * ru1),
							  max(dzmax + ru1, dz1));
			}
			#pragma omp for schedule(static)
			for(k = START(c,3); k <= CELL_SIZE(c,3) - END(c,3) - 1; k++)
			{
				LHS(c,1,k,j,i) = 0;
				LHS(c,2,k,j,i) = - dttz2 * CV(k-1) - dttz1 * RHOS(k-1);
				LHS(c,3,k,j,i) = 1.0 + c2dttz1 * RHOS(k);
				LHS(c,4,k,j,i) =   dttz2 * CV(k+1) - dttz1 * RHOS(k+1);
				LHS(c,5,k,j,i) = 0;
			}
		}

	//---------------------------------------------------------------------
	//      add fourth order dissipation
	//---------------------------------------------------------------------

	#pragma omp parallel default(shared) private(i,j,k)
	{
		if (START(c,3) > 0)
		{
			k = 1;
			#pragma omp for schedule(static)
			for (j = START(c,2); j <= CELL_SIZE(c,2) - END(c,2) - 1; j++)
				for (i = START(c,1); i <= CELL_SIZE(c,1) - END(c,1) - 1; i++)
				{
					LHS(c,3,k,j,i) += comz5;
					LHS(c,4,k,j,i) -= comz4;
					LHS(c,5,k,j,i) += comz1;

					LHS(c,2,k+1,j,i) -= comz4;
					LHS(c,3,k+1,j,i) += comz6;
					LHS(c,4,k+1,j,i) -= comz4;
					LHS(c,5,k+1,j,i) += comz1;
				}
		}

		#pragma omp for schedule(static)
		for (k = 3 * START(c,3); k <= CELL_SIZE(c,3) - 3 * END(c,3) - 1; k++)
			for (j = START(c,2); j <= CELL_SIZE(c,2) - END(c,2) - 1; j++)
				for (i = START(c,1); i <= CELL_SIZE(c,1) - END(c,1) - 1; i++)
				{
					LHS(c,1,k,j,i) += comz1;
					LHS(c,2,k,j,i) -= comz4;
					LHS(c,3,k,j,i) += comz6;
					LHS(c,4,k,j,i) -= comz4;
					LHS(c,5,k,j,i) += comz1;
				}

		if (END(c,3) > 0)
		{
			k = CELL_SIZE(c,3) - 3;
			#pragma omp for schedule(static)
			for (j = START(c,2); j <= CELL_SIZE(c,2) - END(c,2) - 1; j++)
				for (i = START(c,1); i <= CELL_SIZE(c,1) - END(c,1) - 1; i++)
				{
					LHS(c,1,k,j,i) += comz1;
					LHS(c,3,k,j,i) += comz6;
					LHS(c,2,k,j,i) -= comz4;
					LHS(c,4,k,j,i) -= comz4;

					LHS(c,1,k+1,j,i) += comz1;
					LHS(c,2,k+1,j,i) -= comz4;
					LHS(c,3,k+1,j,i) += comz5;
				}
		}

		//---------------------------------------------------------------------
		//      subsequently, fill the other factors (u+c), (u-c)
		//---------------------------------------------------------------------
		#pragma omp for schedule(static)
		for (k = START(c,3); k <= CELL_SIZE(c,3) - END(c,3) - 1; k++)
			for (j = START(c,2); j <= CELL_SIZE(c,2) - END(c,2) - 1; j++)
				for (i = START(c,1); i <= CELL_SIZE(c,1) - END(c,1) - 1; i++)
				{
					LHS(c,1+5,k,j,i)  = LHS(c,1,k,j,i);
					LHS(c,2+5,k,j,i)  = LHS(c,2,k,j,i) - dttz2 * SPEED(c,k-1,j,i);
					LHS(c,3+5,k,j,i)  = LHS(c,3,k,j,i);
					LHS(c,4+5,k,j,i)  = LHS(c,4,k,j,i) + dttz2 * SPEED(c,k+1,j,i);
					LHS(c,5+5,k,j,i)  = LHS(c,5,k,j,i);
					LHS(c,1+10,k,j,i) = LHS(c,1,k,j,i);
					LHS(c,2+10,k,j,i) = LHS(c,2,k,j,i) + dttz2 * SPEED(c,k-1,j,i);
					LHS(c,3+10,k,j,i) = LHS(c,3,k,j,i);
					LHS(c,4+10,k,j,i) = LHS(c,4,k,j,i) - dttz2 * SPEED(c,k+1,j,i);
					LHS(c,5+10,k,j,i) = LHS(c,5,k,j,i);
				}
	}

	return;
}