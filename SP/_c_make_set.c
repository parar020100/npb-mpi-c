#include "sp_data_c.h"
#include "mpinpb_c.h"
#include <mpi.h>
#include <math.h>
#include <stdlib.h>

void make_set_(void)
{
    int p, i, j, size, excess, ierrcode;

    ncells = (int) sqrt(((double) no_nodes) + 0.00001);

    p = ncells;

    CELL_COORD(1,1) = node % p;
    CELL_COORD(1,2) = node / p;
    CELL_COORD(1,3) = 0;

    for (int c = 2; c <= p; c++)
    {
        CELL_COORD(c,1) = (CELL_COORD(c-1,1) + 1) % p;
        CELL_COORD(c,2) = (CELL_COORD(c-1,2) - 1 + p) % p;
        CELL_COORD(c,3) = c - 1;
    }

    for (int dir = 1; dir <= 3; dir++)
        for (int c = 1; c <= p; c++)
            CELL_COORD(c,dir) += 1;

    for (int dir = 1; dir <= 3; dir++)
        for (int c = 1; c <= p; c++)
            SLICE(CELL_COORD(c,dir), dir) = c;

    i = CELL_COORD(1,1) - 1;
    j = CELL_COORD(1,2) - 1;

    PREDECESSOR(1) = ((i - 1 + p) % p) + p * j;
    PREDECESSOR(2) = i                 + p * ((j - 1 + p) % p);
    PREDECESSOR(3) = ((i + 1) % p)     + p * ((j - 1 + p) % p);
    SUCCESSOR(1)   = ((i + 1) % p)     + p * j;
    SUCCESSOR(2)   = i                 + p * ((j + 1) % p);
    SUCCESSOR(3)   = ((i - 1 + p) % p) + p * ((j + 1) % p);

    for (int dir = 1; dir <= 3; dir++)
    {
        size = GRID_POINTS(dir) / p;
        excess = GRID_POINTS(dir) % p;

        for (int c = 1; c <= ncells; c++)
        {
            if(CELL_COORD(c,dir) <= excess)
            {
                CELL_SIZE(c,dir) = size + 1;
                CELL_LOW(c,dir) = (CELL_COORD(c,dir) - 1) * (size + 1);
                CELL_HIGH(c,dir) = CELL_LOW(c,dir) + size;
            }
            else
            {
                CELL_SIZE(c,dir) = size;
                CELL_LOW(c,dir) = excess * (size + 1) +
                    (CELL_COORD(c,dir) - excess - 1) * size;

                CELL_HIGH(c,dir) = CELL_LOW(c,dir) + size - 1;
            }

            if(CELL_SIZE(c,dir) <= 2)
            {
                printf(" Error: Cell size too small. Min size is 3\n");
                fflush(stdout);
                ierrcode = 1;
                MPI_Abort(MPI_COMM_WORLD, ierrcode);
                exit(1);
            }
        }

    }
}