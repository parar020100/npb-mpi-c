#include "sp_data_c.h"

void ninvr_(int *p_c)
{
    double r1, r2, r3, r4, r5, t1, t2;

    int c = *p_c;

    #pragma omp parallel for default(shared) private(r1,r2,r3,r4,r5,t1,t2) schedule(static)
    for(int k = START(c,3); k <= CELL_SIZE(c,3) - END(c,3) - 1; k++)
        for (int j = START(c,2); j <= CELL_SIZE(c,2) - END(c,2) - 1; j++)
            for (int i = START(c,1); i <= CELL_SIZE(c,1) - END(c,1) - 1; i++)
            {
                r1 = RHS(c,1,k,j,i);
                r2 = RHS(c,2,k,j,i);
                r3 = RHS(c,3,k,j,i);
                r4 = RHS(c,4,k,j,i);
                r5 = RHS(c,5,k,j,i);

                t1 = bt * r3;
                t2 = 0.5 * (r4 + r5);

                RHS(c,1,k,j,i) = -r2;
                RHS(c,2,k,j,i) =  r1;
                RHS(c,3,k,j,i) =  bt * (r4 - r5);
                RHS(c,4,k,j,i) = -t1 + t2;
                RHS(c,5,k,j,i) =  t1 + t2;
            }
}