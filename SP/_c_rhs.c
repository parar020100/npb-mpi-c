#include "sp_data_c.h"
#include <math.h>
#include "mpinpb_c.h"

//---------------------------------------------------------------------
//---------------------------------------------------------------------

void compute_rhs_(void)
{

	int c, i, j, k, m;
	double aux, rho_inv, uijk, up1, um1, vijk, vp1, vm1, wijk, wp1, wm1;


	if (timeron)
		timer_start(t_rhs);

	//---------------------------------------------------------------------
	// loop over all cells owned by this node
	//---------------------------------------------------------------------
	for (c = 1; c <= ncells; c++)
	#pragma omp parallel default(shared) private(i,j,k,m,rho_inv,aux, uijk,up1,um1,\
												 vijk,vp1,vm1, wijk,wp1,wm1)
	{

		//---------------------------------------------------------------
		//   compute the reciprocal of density, and the kinetic energy,
		//   and the speed of sound.
		//---------------------------------------------------------------
		#pragma omp for schedule(static) nowait
		for (k = -1; k <= CELL_SIZE(c,3); k++)
			for (j = -1; j <= CELL_SIZE(c,2); j++)
				for (i = -1; i <= CELL_SIZE(c,1); i++)
				{
					rho_inv = 1.0 / U(c,1,k,j,i);
					RHO_I(c,k,j,i) = rho_inv;
					US(c,k,j,i) = U(c,2,k,j,i) * rho_inv;
					VS(c,k,j,i) = U(c,3,k,j,i) * rho_inv;
					WS(c,k,j,i) = U(c,4,k,j,i) * rho_inv;
					SQUARE(c,k,j,i) = 0.5 * (U(c,2,k,j,i) * U(c,2,k,j,i) + U(c,3,k,j,i) * U(c,3,k,j,i) + U(c,4,k,j,i) * U(c,4,k,j,i)) * rho_inv;
					QS(c,k,j,i) = SQUARE(c,k,j,i) * rho_inv;
					//------------------------------------------------------
					//   (don't need speed and ainx until the lhs computation)
					//------------------------------------------------------
					aux = c1c2 * rho_inv * (U(c,5,k,j,i) - SQUARE(c,k,j,i));
					aux = sqrt(aux);
					SPEED(c,k,j,i) = aux;
					AINV(c,k,j,i)  = 1.0 / aux;
				}

		//----------------------------------------------------------------------
		//   copy the exact forcing term to the right hand side; because
		//   this forcing term is known, we can store it on the whole of every
		//   cell,  including the boundary
		//----------------------------------------------------------------------
		#pragma omp for schedule(static)
		for (m = 1; m <= 5; m++)
			for(k = 0; k <= CELL_SIZE(c,3) - 1; k++)
				for(j = 0; j <= CELL_SIZE(c,2) - 1; j++)
					for(i = 0; i <= CELL_SIZE(c,1) - 1; i++)
						RHS(c,m,k,j,i) = FORCING(c, m, k, j, i);


		//---------------------------------------------------------------
		//   compute xi-direction fluxes
		//---------------------------------------------------------------
		#pragma omp for schedule(static)
		for (k = START(c,3); k <= CELL_SIZE(c,3) - END(c,3) - 1; k++)
			for (j = START(c,2); j <= CELL_SIZE(c,2) - END(c,2) - 1; j++)
				for (i = START(c,1); i <= CELL_SIZE(c,1) - END(c,1) - 1; i++)
				{
					uijk = US(c,k,j,i);
					up1  = US(c,k,j,i+1);
					um1  = US(c,k,j,i-1);

					RHS(c,1,k,j,i) += dx1tx1 * (U(c,1,k,j,i+1) - 2.0 * U(c,1,k,j,i) + U(c,1,k,j,i-1));
					RHS(c,1,k,j,i) -= tx2 * (U(c,2,k,j,i+1) - U(c,2,k,j,i-1));

					RHS(c,2,k,j,i) += dx2tx1 * (U(c,2,k,j,i+1) - 2.0 * U(c,2,k,j,i) + U(c,2,k,j,i-1));
					RHS(c,2,k,j,i) += xxcon2 * con43 * (up1 - 2.0 * uijk + um1);

					RHS(c,2,k,j,i) -= tx2 * (U(c,2,k,j,i+1) * up1 - U(c,2,k,j,i-1) * um1);
					RHS(c,2,k,j,i) -= tx2 * c2 * (U(c,5,k,j,i+1) - SQUARE(c,k,j,i+1));
					RHS(c,2,k,j,i) += tx2 * c2 * (U(c,5,k,j,i-1) - SQUARE(c,k,j,i-1));

					RHS(c,3,k,j,i) += dx3tx1 * (U(c,3,k,j,i+1) - 2.0 * U(c,3,k,j,i) + U(c,3,k,j,i-1));
					RHS(c,3,k,j,i) += xxcon2 * (VS(c,k,j,i+1) - 2.0 * VS(c,k,j,i) + VS(c,k,j,i-1));
					RHS(c,3,k,j,i) -= tx2 * (U(c,3,k,j,i+1) * up1 - U(c,3,k,j,i-1) * um1);

					RHS(c,4,k,j,i) += dx4tx1 * (U(c,4,k,j,i+1) - 2.0 * U(c,4,k,j,i) + U(c,4,k,j,i-1));
					RHS(c,4,k,j,i) += xxcon2 * (WS(c,k,j,i+1) - 2.0 * WS(c,k,j,i) + WS(c,k,j,i-1));
					RHS(c,4,k,j,i) -= tx2 * (U(c,4,k,j,i+1) * up1 - U(c,4,k,j,i-1) * um1);

					RHS(c,5,k,j,i) += dx5tx1 * (U(c,5,k,j,i+1) - 2.0 * U(c,5,k,j,i) + U(c,5,k,j,i-1));
					RHS(c,5,k,j,i) += xxcon3 * (QS(c,k,j,i+1) - 2.0 * QS(c,k,j,i) + QS(c,k,j,i-1));
					RHS(c,5,k,j,i) += xxcon4 * (up1 * up1 - 2.0 * uijk * uijk + um1 * um1);
					RHS(c,5,k,j,i) += xxcon5 * U(c,5,k,j,i+1) * RHO_I(c,k,j,i+1);
					RHS(c,5,k,j,i) -= xxcon5 * 2.0 * U(c,5,k,j,i) * RHO_I(c,k,j,i);
					RHS(c,5,k,j,i) += xxcon5 * U(c,5,k,j,i-1) * RHO_I(c,k,j,i-1);
					RHS(c,5,k,j,i) -= tx2 * up1 * (c1 * U(c,5,k,j,i+1) - c2 * SQUARE(c,k,j,i+1));
					RHS(c,5,k,j,i) += tx2 * um1 * (c1 * U(c,5,k,j,i-1) - c2 * SQUARE(c,k,j,i-1));
				}

		#pragma omp single
		{
		//---------------------------------------------------------------
		//   add fourth order xi-direction dissipation
		//---------------------------------------------------------------
		if (START(c,1) > 0)
		{
			i = 1;
			for (m = 1; m <= 5; m++)
				for (k = START(c,3); k <= CELL_SIZE(c,3) - END(c,3) - 1; k++)
					for (j = START(c,2); j <= CELL_SIZE(c,2) - END(c,2) - 1; j++)
						RHS(c,m,k,j,i) -= dssp * (5.0 * U(c,m,k,j,i) - 4.0 * U(c,m,k,j,i+1) + U(c,m,k,j,i+2));

			i = 2;
			for (m = 1; m <= 5; m++)
				for (k = START(c,3); k <= CELL_SIZE(c,3) - END(c,3) - 1; k++)
					for (j = START(c,2); j <= CELL_SIZE(c,2) - END(c,2) - 1; j++)
					{
						RHS(c,m,k,j,i) -= dssp *
							(-4.0 * U(c,m,k,j,i-1) + 6.0 * U(c,m,k,j,i) - 4.0 * U(c,m,k,j,i+1) + U(c,m,k,j,i+2));
					}
		}
		}

		#pragma omp for schedule(static)
		for (m = 1; m <= 5; m++)
			for (k = START(c,3); k <= CELL_SIZE(c,3) - END(c,3) - 1; k++)
				for (j = START(c,2); j <= CELL_SIZE(c,2) - END(c,2) - 1; j++)
					for (i = 3 * START(c,1); i <= CELL_SIZE(c,1) - 3 * END(c,1) - 1; i++)
						RHS(c,m,k,j,i) += - dssp *
							(U(c,m,k,j,i-2) - 4.0 * U(c,m,k,j,i-1) +
							 6.0 * U(c,m,k,j,i) - 4.0 * U(c,m,k,j,i+1) + U(c,m,k,j,i+2));

		#pragma omp single
		{
		if (END(c,1) > 0)
		{
			i = CELL_SIZE(c,1) - 3;
			for (m = 1; m <= 5; m++)
				for (k = START(c,3); k <= CELL_SIZE(c,3) - END(c,3) - 1; k++)
					for (j = START(c,2); j <= CELL_SIZE(c,2) - END(c,2) - 1; j++)
						RHS(c,m,k,j,i) -= dssp *
							(U(c,m,k,j,i-2) - 4.0 * U(c,m,k,j,i-1) + 6.0 * U(c,m,k,j,i) - 4.0 * U(c,m,k,j,i+1));

			i = CELL_SIZE(c,1) - 2;
			for (m = 1; m <= 5; m++)
				for (k = START(c,3); k <= CELL_SIZE(c,3) - END(c,3) - 1; k++)
					for (j = START(c,2); j <= CELL_SIZE(c,2) - END(c,2) - 1; j++)
						RHS(c,m,k,j,i) -= dssp * (U(c,m,k,j,i-2) - 4.0 * U(c,m,k,j,i-1) + 5.0 * U(c,m,k,j,i));

		}
		} // #pragma omp single

		//---------------------------------------------------------------
		//   compute eta-direction fluxes
		//---------------------------------------------------------------
		#pragma omp for schedule(static)
		for (k = START(c,3); k <= CELL_SIZE(c,3) - END(c,3) - 1; k++)
			for (j = START(c,2); j <= CELL_SIZE(c,2) - END(c,2) - 1; j++)
				for (i = START(c,1); i <= CELL_SIZE(c,1) - END(c,1) - 1; i++)
				{
					vijk = VS(c,k,j,i);
					vp1  = VS(c,k,j+1,i);
					vm1  = VS(c,k,j-1,i);

					RHS(c,1,k,j,i) += dy1ty1 * (U(c,1,k,j+1,i) - 2.0 * U(c,1,k,j,i) + U(c,1,k,j-1,i));
					RHS(c,1,k,j,i) -= ty2 * (U(c,3,k,j+1,i) - U(c,3,k,j-1,i));

					RHS(c,2,k,j,i) += dy2ty1 * (U(c,2,k,j+1,i) - 2.0 * U(c,2,k,j,i) + U(c,2,k,j-1,i));
					RHS(c,2,k,j,i) += yycon2 * (US(c,k,j+1,i) - 2.0 * US(c,k,j,i) + US(c,k,j-1,i));
					RHS(c,2,k,j,i) -= ty2 * (U(c,2,k,j+1,i) * vp1 - U(c,2,k,j-1,i) * vm1);

					RHS(c,3,k,j,i) += dy3ty1 * (U(c,3,k,j+1,i) - 2.0 * U(c,3,k,j,i) + U(c,3,k,j-1,i));
					RHS(c,3,k,j,i) += yycon2 * con43 * (vp1 - 2.0 * vijk + vm1);
					RHS(c,3,k,j,i) -= ty2 * (U(c,3,k,j+1,i) * vp1 - U(c,3,k,j-1,i) * vm1);
					RHS(c,3,k,j,i) -= ty2 * c2 * (U(c,5,k,j+1,i) - SQUARE(c,k,j+1,i));
					RHS(c,3,k,j,i) += ty2 * c2 * (U(c,5,k,j-1,i) - SQUARE(c,k,j-1,i));

					RHS(c,4,k,j,i) += dy4ty1 * (U(c,4,k,j+1,i) - 2.0 * U(c,4,k,j,i) + U(c,4,k,j-1,i));
					RHS(c,4,k,j,i) += yycon2 * (WS(c,k,j+1,i) - 2.0 * WS(c,k,j,i) + WS(c,k,j-1,i));
					RHS(c,4,k,j,i) -= ty2 * (U(c,4,k,j+1,i) * vp1 - U(c,4,k,j-1,i) * vm1);

					RHS(c,5,k,j,i) += dy5ty1 * (U(c,5,k,j+1,i) - 2.0 * U(c,5,k,j,i) + U(c,5,k,j-1,i));
					RHS(c,5,k,j,i) += yycon3 * (QS(c,k,j+1,i) - 2.0 * QS(c,k,j,i) + QS(c,k,j-1,i));
					RHS(c,5,k,j,i) += yycon4 * (vp1 * vp1 - 2.0 * vijk * vijk + vm1 * vm1);
					RHS(c,5,k,j,i) += yycon5 * U(c,5,k,j+1,i) * RHO_I(c,k,j+1,i);
					RHS(c,5,k,j,i) -= yycon5 * 2.0 * U(c,5,k,j,i) * RHO_I(c,k,j,i);
					RHS(c,5,k,j,i) += yycon5 * U(c,5,k,j-1,i) * RHO_I(c,k,j-1,i);
					RHS(c,5,k,j,i) -= ty2 * vp1 * (c1 * U(c,5,k,j+1,i) - c2 * SQUARE(c,k,j+1,i));
					RHS(c,5,k,j,i) += ty2 * vm1 * (c1 * U(c,5,k,j-1,i) - c2 * SQUARE(c,k,j-1,i));
				}

		#pragma omp single
		{
		//---------------------------------------------------------------
		//   add fourth order eta-direction dissipation
		//---------------------------------------------------------------
		if (START(c,2) > 0)
		{
			j = 1;
			for (m = 1; m <= 5; m++)
				for (k = START(c,3); k <= CELL_SIZE(c,3) - END(c,3) - 1; k++)
					for (i = START(c,1); i <= CELL_SIZE(c,1) - END(c,1) - 1; i++)
						RHS(c,m,k,j,i) -= dssp * ( 5.0 * U(c,m,k,j,i) - 4.0 * U(c,m,k,j+1,i) + U(c,m,k,j+2,i));

			j = 2;
			for (m = 1; m <= 5; m++)
				for (k = START(c,3); k <= CELL_SIZE(c,3) - END(c,3) - 1; k++)
					for (i = START(c,1); i <= CELL_SIZE(c,1) - END(c,1) - 1; i++)
						RHS(c,m,k,j,i) -= dssp *
							(-4.0 * U(c,m,k,j-1,i) + 6.0 * U(c,m,k,j,i) - 4.0 * U(c,m,k,j+1,i) + U(c,m,k,j+2,i));

		}
		}

		#pragma omp for schedule(static)
		for (m = 1; m <= 5; m++)
			for (k = START(c,3); k <= CELL_SIZE(c,3) - END(c,3) - 1; k++)
				for(j = 3 * START(c,2); j <= CELL_SIZE(c,2) - 3 * END(c,2) - 1; j++)
					for (i = START(c,1); i <= CELL_SIZE(c,1) - END(c,1) - 1; i++)
						RHS(c,m,k,j,i) -= dssp *
							(U(c,m,k,j-2,i) - 4.0 * U(c,m,k,j-1,i)
							 + 6.0 * U(c,m,k,j,i) - 4.0 * U(c,m,k,j+1,i) + U(c,m,k,j+2,i));

		#pragma omp single
		{
		if (END(c,2) > 0)
		{
			j = CELL_SIZE(c,2) - 3;
			for (m = 1; m <= 5; m++)
				for (k = START(c,3); k <= CELL_SIZE(c,3) - END(c,3) - 1; k++)
					for (i = START(c,1); i <= CELL_SIZE(c,1) - END(c,1) - 1; i++)
						RHS(c,m,k,j,i) -= dssp *
							(U(c,m,k,j-2,i) - 4.0 * U(c,m,k,j-1,i) + 6.0 * U(c,m,k,j,i) - 4.0 * U(c,m,k,j+1,i));

			j = CELL_SIZE(c,2) - 2;
			for (m = 1; m <= 5; m++)
				for (k = START(c,3); k <= CELL_SIZE(c,3) - END(c,3) - 1; k++)
					for (i = START(c,1); i <= CELL_SIZE(c,1) - END(c,1) - 1; i++)
						RHS(c,m,k,j,i) -= dssp * (U(c,m,k,j-2,i) - 4.0 * U(c,m,k,j-1,i) + 5.0 * U(c,m,k,j,i));
		}
		} // #pragma omp single
		//---------------------------------------------------------------
		//   compute zeta-direction fluxes
		//---------------------------------------------------------------
		#pragma omp for schedule(static)
		for (k = START(c,3); k <= CELL_SIZE(c,3) - END(c,3) - 1; k++)
			for (j = START(c,2); j <= CELL_SIZE(c,2) - END(c,2) - 1; j++)
				for (i = START(c,1); i <= CELL_SIZE(c,1) - END(c,1) - 1; i++)
				{
					wijk = WS(c,k,j,i);
					wp1  = WS(c,k+1,j,i);
					wm1  = WS(c,k-1,j,i);

					RHS(c,1,k,j,i) += dz1tz1 *
						(U(c,1,k+1,j,i) - 2.0 * U(c,1,k,j,i) + U(c,1,k-1,j,i)) - tz2 * (U(c,4,k+1,j,i) - U(c,4,k-1,j,i));

					RHS(c,2,k,j,i) += dz2tz1 * (U(c,2,k+1,j,i) - 2.0 * U(c,2,k,j,i) + U(c,2,k-1,j,i));
					RHS(c,2,k,j,i) += zzcon2 * (US(c,k+1,j,i) - 2.0 * US(c,k,j,i) + US(c,k-1,j,i));
					RHS(c,2,k,j,i) -= tz2 * (U(c,2,k+1,j,i) * wp1 - U(c,2,k-1,j,i) * wm1);

					RHS(c,3,k,j,i) += dz3tz1 * (U(c,3,k+1,j,i) - 2.0 * U(c,3,k,j,i) + U(c,3,k-1,j,i));
					RHS(c,3,k,j,i) += zzcon2 * (VS(c,k+1,j,i) - 2.0 * VS(c,k,j,i) + VS(c,k-1,j,i));
					RHS(c,3,k,j,i) -= tz2 * (U(c,3,k+1,j,i) * wp1 - U(c,3,k-1,j,i) * wm1);

					RHS(c,4,k,j,i) += dz4tz1 * (U(c,4,k+1,j,i) - 2.0 * U(c,4,k,j,i) + U(c,4,k-1,j,i));
					RHS(c,4,k,j,i) += zzcon2 * con43 * (wp1 - 2.0 * wijk + wm1);
					RHS(c,4,k,j,i) -= tz2 * (U(c,4,k+1,j,i) * wp1 - U(c,4,k-1,j,i) * wm1);
					RHS(c,4,k,j,i) -= tz2 * c2 * (U(c,5,k+1,j,i) - SQUARE(c,k+1,j,i));
					RHS(c,4,k,j,i) += tz2 * c2 * (U(c,5,k-1,j,i) - SQUARE(c,k-1,j,i));

					RHS(c,5,k,j,i) += dz5tz1 * (U(c,5,k+1,j,i) - 2.0 * U(c,5,k,j,i) + U(c,5,k-1,j,i));
					RHS(c,5,k,j,i) += zzcon3 * (QS(c,k+1,j,i) - 2.0 * QS(c,k,j,i) + QS(c,k-1,j,i));
					RHS(c,5,k,j,i) += zzcon4 * (wp1 * wp1 - 2.0 * wijk * wijk + wm1 * wm1);
					RHS(c,5,k,j,i) += zzcon5 * U(c,5,k+1,j,i) * RHO_I(c,k+1,j,i);
					RHS(c,5,k,j,i) -= zzcon5 * 2.0 * U(c,5,k,j,i) * RHO_I(c,k,j,i);
					RHS(c,5,k,j,i) += zzcon5 * U(c,5,k-1,j,i) * RHO_I(c,k-1,j,i);
					RHS(c,5,k,j,i) -= tz2 * wp1 * (c1 * U(c,5,k+1,j,i) - c2 * SQUARE(c,k+1,j,i));
					RHS(c,5,k,j,i) += tz2 * wm1 * (c1 * U(c,5,k-1,j,i) - c2 * SQUARE(c,k-1,j,i));
				}

		//---------------------------------------------------------------
		//   add fourth order zeta-direction dissipation
		//---------------------------------------------------------------
		#pragma omp single
		{
		if (START(c,3) > 0)
		{
			k = 1;
			for (m = 1; m <= 5; m++)
				for (j = START(c,2); j <= CELL_SIZE(c,2) - END(c,2) - 1; j++)
					for (i = START(c,1); i <= CELL_SIZE(c,1) - END(c,1) - 1; i++)
						RHS(c,m,k,j,i) -= dssp * ( 5.0 * U(c,m,k,j,i) - 4.0 * U(c,m,k+1,j,i) + U(c,m,k+2,j,i));

			k = 2;
			for (m = 1; m <= 5; m++)
				for (j = START(c,2); j <= CELL_SIZE(c,2) - END(c,2) - 1; j++)
					for (i = START(c,1); i <= CELL_SIZE(c,1) - END(c,1) - 1; i++)
						RHS(c,m,k,j,i) -= dssp *
							(-4.0 * U(c,m,k-1,j,i) + 6.0 * U(c,m,k,j,i) - 4.0 * U(c,m,k+1,j,i) + U(c,m,k+2,j,i));
		}
		}

		#pragma omp for schedule(static)
		for (m = 1; m <= 5; m++)
			for (k = 3 * START(c,3); k <= CELL_SIZE(c,3) - 3 * END(c,3) - 1; k++)
				for (j = START(c,2); j <= CELL_SIZE(c,2) - END(c,2) - 1; j++)
					for (i = START(c,1); i <= CELL_SIZE(c,1) - END(c,1) - 1; i++)
						RHS(c,m,k,j,i) -= dssp *
							(U(c,m,k-2,j,i) - 4.0 * U(c,m,k-1,j,i) + 6.0 * U(c,m,k,j,i)
							 - 4.0 * U(c,m,k+1,j,i) + U(c,m,k+2,j,i));

		#pragma omp single
		{
		if (END(c,3) > 0)
		{
			k = CELL_SIZE(c,3) - 3;
			for (m = 1; m <= 5; m++)
				for (j = START(c,2); j <= CELL_SIZE(c,2) - END(c,2) - 1; j++)
					for (i = START(c,1); i <= CELL_SIZE(c,1) - END(c,1) - 1; i++)
						RHS(c,m,k,j,i) -= dssp *
							(U(c,m,k-2,j,i) - 4.0 * U(c,m,k-1,j,i) + 6.0 * U(c,m,k,j,i) - 4.0 * U(c,m,k+1,j,i));

			k = CELL_SIZE(c,3) - 2;
			for (m = 1; m <= 5; m++)
				for (j = START(c,2); j <= CELL_SIZE(c,2) - END(c,2) - 1; j++)
					for (i = START(c,1); i <= CELL_SIZE(c,1) - END(c,1) - 1; i++)
						RHS(c,m,k,j,i) -= dssp * (U(c,m,k-2,j,i) - 4.0 * U(c,m,k-1,j,i) + 5.0 * U(c,m,k,j,i));
		}
		}

		#pragma omp for schedule(static) nowait
		for (m = 1; m <= 5; m++)
			for (k = START(c,3); k <= CELL_SIZE(c,3) - END(c,3) - 1; k++)
				for (j = START(c,2); j <= CELL_SIZE(c,2) - END(c,2) - 1; j++)
					for (i = START(c,1); i <= CELL_SIZE(c,1) - END(c,1) - 1; i++)
						RHS(c,m,k,j,i) *= dt;


	}

	if (timeron)
		timer_stop(t_rhs);

	return;
}


