#include "sp_data_c.h"

void txinvr_(void)
{

    double t1, t2, t3, ac, ru1, uu, vv, ww, r1, r2, r3, r4, r5, ac2inv;

    for (int c = 1; c <= ncells; c++)
        #pragma omp parallel for default(shared) private(r1,r2,r3,r4,r5,t1,t2,t3,ru1, \
                                                         uu,vv,ww,ac,ac2inv) schedule(static)
        for(int k = START(c,3); k <= CELL_SIZE(c,3) - END(c,3) - 1; k++)
            for (int j = START(c,2); j <= CELL_SIZE(c,2) - END(c,2) - 1; j++)
                for (int i = START(c,1); i <= CELL_SIZE(c,1) - END(c,1) - 1; i++)
                {
                    ru1    = RHO_I(c,k,j,i);
                    uu     = US(c,k,j,i);
                    vv     = VS(c,k,j,i);
                    ww     = WS(c,k,j,i);
                    ac     = SPEED(c,k,j,i);
                    ac2inv = AINV(c,k,j,i) * AINV(c,k,j,i);

                    r1 = RHS(c,1,k,j,i);
                    r2 = RHS(c,2,k,j,i);
                    r3 = RHS(c,3,k,j,i);
                    r4 = RHS(c,4,k,j,i);
                    r5 = RHS(c,5,k,j,i);

                    t1 = c2 * ac2inv * \
                        (QS(c,k,j,i)*r1 - uu*r2 - vv*r3 - ww*r4 + r5);
                    t2 = bt * ru1 * (uu * r1 - r2);
                    t3 = (bt * ru1 * ac) * t1;

                    RHS(c,1,k,j,i) =  r1 - t1;
                    RHS(c,2,k,j,i) = -ru1 * (ww * r1 - r4);
                    RHS(c,3,k,j,i) =  ru1 * (vv * r1 - r3);
                    RHS(c,4,k,j,i) = -t2 + t3;
                    RHS(c,5,k,j,i) =  t2 + t3;
                }

}