#include "sp_data_c.h"

void tzetar_(int *p_c)
{
    int c = *p_c;

    double t1, t2, t3, ac, xvel, yvel, zvel, r1, r2, r3, r4, r5, btuz,
           acinv, ac2u, uzik1;

    #pragma omp parallel for default(shared) private(r1,r2,r3,r4,r5,t1,t2,t3,xvel,yvel,zvel, \
                                                     ac,acinv,ac2u,uzik1,btuz) schedule(static)
    for(int k = START(c,3); k <= CELL_SIZE(c,3) - END(c,3) - 1; k++)
        for (int j = START(c,2); j <= CELL_SIZE(c,2) - END(c,2) - 1; j++)
            for (int i = START(c,1); i <= CELL_SIZE(c,1) - END(c,1) - 1; i++)
            {
                xvel  = US(c,k,j,i);
                yvel  = VS(c,k,j,i);
                zvel  = WS(c,k,j,i);
                ac    = SPEED(c,k,j,i);
                acinv = AINV(c,k,j,i);

                ac2u = ac * ac;

                r1 = RHS(c,1,k,j,i);
                r2 = RHS(c,2,k,j,i);
                r3 = RHS(c,3,k,j,i);
                r4 = RHS(c,4,k,j,i);
                r5 = RHS(c,5,k,j,i);

                uzik1 = U(c,1,k,j,i);
                btuz  = bt * uzik1;

                t1 = btuz * acinv * (r4 + r5);
                t2 = r3 + t1;
                t3 = btuz * (r4 - r5);

                RHS(c,1,k,j,i) =  t2;
                RHS(c,2,k,j,i) = -uzik1 * r2 + xvel * t2;
                RHS(c,3,k,j,i) =  uzik1 * r1 + yvel * t2;
                RHS(c,4,k,j,i) =  zvel * t2 + t3;
                RHS(c,5,k,j,i) =  uzik1 * (-xvel * r2 + yvel * r1);

                RHS(c,5,k,j,i) += QS(c,k,j,i) * t2;
                RHS(c,5,k,j,i) += c2iv * ac2u * t1 + zvel * t3;
            }

}