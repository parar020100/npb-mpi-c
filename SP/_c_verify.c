#include "sp_data_c.h"
#include "mpinpb_c.h"
#include <math.h>


//---------------------------------------------------------------------
//  set problem class based on problem size
//---------------------------------------------------------------------

void set_class_(int *p_no_time_steps, char *p_class)
{
	int no_time_steps = *p_no_time_steps;
	int gp = GRID_POINTS(1);

	*p_class = 'U';

	if (gp != GRID_POINTS(2) || gp != GRID_POINTS(3))
		return;

	switch(gp)
	{
		case 12:
			if (no_time_steps == 100)
				*p_class = 'S';
			return;
		case 36:
			if (no_time_steps == 400)
				*p_class = 'W';
			return;
		case 64:
			if (no_time_steps == 400)
				*p_class = 'A';
			return;
		case 102:
			if (no_time_steps == 400)
				*p_class = 'B';
			return;
		case 162:
			if (no_time_steps == 400)
				*p_class = 'C';
			return;
		case 408:
			if (no_time_steps == 500)
				*p_class = 'D';
			return;
		case 1020:
			if (no_time_steps == 500)
				*p_class = 'E';
			return;
		case 2560:
			if (no_time_steps == 500)
				*p_class = 'F';
			return;
	}
}

//---------------------------------------------------------------------
//  verification routine
//---------------------------------------------------------------------
void verify_(char *p_class, logical *p_verified)
{
	double xcrref[5], xceref[5], xcrdif[5], xcedif[5], epsilon, xce[5], xcr[5], dtref;
	logical verified = *p_verified;
	char class = *p_class;
	int m;

	//---------------------------------------------------------------------
	//   tolerance level
	//---------------------------------------------------------------------
	epsilon = 1.0E-08;

	//---------------------------------------------------------------------
	//   compute the error norm and the residual norm, and exit if not printing
	//---------------------------------------------------------------------

	error_norm_(xce);
	copy_faces_();
	rhs_norm_(xcr);

	for (m = 0; m < 5; m++)
		xcr[m] /= dt;

	if (node != 0)
		return;

	verified = true;

	for (m = 0; m < 5; m++)
	{
		xcrref[m] = 1.0;
		xceref[m] = 1.0;
	}


	//---------------------------------------------------------------------
	//    reference data for 12X12X12 grids after 100 time steps, with DT = 1.50d-02
	//---------------------------------------------------------------------
	if ( class == 'S' )
	{

		dtref = 1.5E-2;

		//---------------------------------------------------------------------
		//    Reference values of RMS-norms of residual.
		//---------------------------------------------------------------------
		xcrref[0] = 2.7470315451339479E-02;
		xcrref[1] = 1.0360746705285417E-02;
		xcrref[2] = 1.6235745065095532E-02;
		xcrref[3] = 1.5840557224455615E-02;
		xcrref[4] = 3.4849040609362460E-02;

		//---------------------------------------------------------------------
		//    Reference values of RMS-norms of solution error.
		//---------------------------------------------------------------------
		xceref[0] = 2.7289258557377227E-05;
		xceref[1] = 1.0364446640837285E-05;
		xceref[2] = 1.6154798287166471E-05;
		xceref[3] = 1.5750704994480102E-05;
		xceref[4] = 3.4177666183390531E-05;
	}
	//---------------------------------------------------------------------
	//    reference data for 36X36X36 grids after 400 time steps, with DT = 1.5d-03
	//---------------------------------------------------------------------
	else if ( class == 'W' )
	{

		dtref = 1.5E-3;

		//-----------------------------7----------------------------------------
		//    Reference values of RMS-norms of residual.
		//---------------------------------------------------------------------
		xcrref[0] = 0.1893253733584E-02;
		xcrref[1] = 0.1717075447775E-03;
		xcrref[2] = 0.2778153350936E-03;
		xcrref[3] = 0.2887475409984E-03;
		xcrref[4] = 0.3143611161242E-02;

		//---------------------------------------------------------------------
		//    Reference values of RMS-norms of solution error.
		//---------------------------------------------------------------------
		xceref[0] = 0.7542088599534E-04;
		xceref[1] = 0.6512852253086E-05;
		xceref[2] = 0.1049092285688E-04;
		xceref[3] = 0.1128838671535E-04;
		xceref[4] = 0.1212845639773E-03;

	}
	//---------------------------------------------------------------------
	//    reference data for 64X64X64 grids after 400 time steps, with DT = 1.5d-03
	//---------------------------------------------------------------------
	else if ( class == 'A' )
	{

		dtref = 1.5E-3;

		//---------------------------------------------------------------------
		//    Reference values of RMS-norms of residual.
		//---------------------------------------------------------------------
		xcrref[0] = 2.4799822399300195;
		xcrref[1] = 1.1276337964368832;
		xcrref[2] = 1.5028977888770491;
		xcrref[3] = 1.4217816211695179;
		xcrref[4] = 2.1292113035138280;

		//---------------------------------------------------------------------
		//    Reference values of RMS-norms of solution error.
		//---------------------------------------------------------------------
		xceref[0] = 1.0900140297820550E-04;
		xceref[1] = 3.7343951769282091E-05;
		xceref[2] = 5.0092785406541633E-05;
		xceref[3] = 4.7671093939528255E-05;
		xceref[4] = 1.3621613399213001E-04;

	}
	//---------------------------------------------------------------------
	//    reference data for 102X102X102 grids after 400 time steps,
	//    with DT = 1.0d-03
	//---------------------------------------------------------------------
	else if ( class == 'B' )
	{

		dtref = 1.0E-3;

		//---------------------------------------------------------------------
		//    Reference values of RMS-norms of residual.
		//---------------------------------------------------------------------
		xcrref[0] = 0.6903293579998E2;
		xcrref[1] = 0.3095134488084E2;
		xcrref[2] = 0.4103336647017E2;
		xcrref[3] = 0.3864769009604E2;
		xcrref[4] = 0.5643482272596E2;

		//---------------------------------------------------------------------
		//    Reference values of RMS-norms of solution error.
		//---------------------------------------------------------------------
		xceref[0] = 0.9810006190188E-02;
		xceref[1] = 0.1022827905670E-02;
		xceref[2] = 0.1720597911692E-02;
		xceref[3] = 0.1694479428231E-02;
		xceref[4] = 0.1847456263981E-01;

	}
	//---------------------------------------------------------------------
	//    reference data for 162X162X162 grids after 400 time steps,
	//    with DT = 0.67d-03
	//---------------------------------------------------------------------
	else if ( class == 'C' )
	{

		dtref = 0.67E-3;

		//---------------------------------------------------------------------
		//    Reference values of RMS-norms of residual.
		//---------------------------------------------------------------------
		xcrref[0] = 0.5881691581829E+03;
		xcrref[1] = 0.2454417603569E+03;
		xcrref[2] = 0.3293829191851E+03;
		xcrref[3] = 0.3081924971891E+03;
		xcrref[4] = 0.4597223799176E+03;

		//---------------------------------------------------------------------
		//    Reference values of RMS-norms of solution error.
		//---------------------------------------------------------------------
		xceref[0] = 0.2598120500183E+00;
		xceref[1] = 0.2590888922315E-01;
		xceref[2] = 0.5132886416320E-01;
		xceref[3] = 0.4806073419454E-01;
		xceref[4] = 0.5483377491301E+00;

	}
	//---------------------------------------------------------------------
	//    reference data for 408X408X408 grids after 500 time steps,
	//    with DT = 0.3d-03
	//---------------------------------------------------------------------
	else if ( class == 'D' )
	{

		dtref = 0.30E-3;

		//---------------------------------------------------------------------
		//    Reference values of RMS-norms of residual.
		//---------------------------------------------------------------------
		xcrref[0] = 0.1044696216887E+05;
		xcrref[1] = 0.3204427762578E+04;
		xcrref[2] = 0.4648680733032E+04;
		xcrref[3] = 0.4238923283697E+04;
		xcrref[4] = 0.7588412036136E+04;

		//---------------------------------------------------------------------
		//    Reference values of RMS-norms of solution error.
		//---------------------------------------------------------------------
		xceref[0] = 0.5089471423669E+01;
		xceref[1] = 0.5323514855894E+00;
		xceref[2] = 0.1187051008971E+01;
		xceref[3] = 0.1083734951938E+01;
		xceref[4] = 0.1164108338568E+02;

	}
	//---------------------------------------------------------------------
	//    reference data for 1020X1020X1020 grids after 500 time steps,
	//    with DT = 0.1d-03
	//---------------------------------------------------------------------
	else if ( class == 'E' )
	{

		dtref = 0.10E-3;

		//---------------------------------------------------------------------
		//    Reference values of RMS-norms of residual.
		//---------------------------------------------------------------------
		xcrref[0] = 0.6255387422609E+05;
		xcrref[1] = 0.1495317020012E+05;
		xcrref[2] = 0.2347595750586E+05;
		xcrref[3] = 0.2091099783534E+05;
		xcrref[4] = 0.4770412841218E+05;

		//---------------------------------------------------------------------
		//    Reference values of RMS-norms of solution error.
		//---------------------------------------------------------------------
		xceref[0] = 0.6742735164909E+02;
		xceref[1] = 0.5390656036938E+01;
		xceref[2] = 0.1680647196477E+02;
		xceref[3] = 0.1536963126457E+02;
		xceref[4] = 0.1575330146156E+03;

	}
	//---------------------------------------------------------------------
	//    reference data for 2560X2560X2560 grids after 500 time steps,
	//    with DT = 0.1d-03
	//---------------------------------------------------------------------
	else if ( class == 'F' )
	{

		dtref = 0.15E-4;

		//---------------------------------------------------------------------
		//    Reference values of RMS-norms of residual.
		//---------------------------------------------------------------------
		xcrref[0] = 0.9281628449462E+05;
		xcrref[1] = 0.2230152287675E+05;
		xcrref[2] = 0.3493102358632E+05;
		xcrref[3] = 0.3114096186689E+05;
		xcrref[4] = 0.7424426448298E+05;

		//---------------------------------------------------------------------
		//    Reference values of RMS-norms of solution error.
		//---------------------------------------------------------------------
		xceref[0] = 0.2683717702444E+03;
		xceref[1] = 0.2030647554028E+02;
		xceref[2] = 0.6734864248234E+02;
		xceref[3] = 0.5947451301640E+02;
		xceref[4] = 0.5417636652565E+03;
	}
	else
		verified = false;

	//---------------------------------------------------------------------
	//    verification test for residuals if gridsize is one of
	//    the defined grid sizes above (class .ne. 'U')
	//---------------------------------------------------------------------

	//---------------------------------------------------------------------
	//    Compute the difference of solution values and the known reference values.
	//---------------------------------------------------------------------

	for (m = 0; m < 5; m++)
	{
		xcrdif[m] = fabs( (xcr[m] - xcrref[m]) / xcrref[m] );
		xcedif[m] = fabs( (xce[m] - xceref[m]) / xceref[m] );
	}

	//---------------------------------------------------------------------
	//    Output the comparison of computed results to known cases.
	//---------------------------------------------------------------------

	if (class != 'U')
	{
		printf(" Verification being performed for class %c\n", class);
		printf(" accuracy setting for epsilon = %20.13E\n", epsilon);

		verified = ( fabs(dt - dtref) <= epsilon );
		if (!verified)
		{
			class = 'U';
			printf(" DT does not match the reference value of %15.8E\n", dtref);
		}
	}
	else
		printf(" Unknown class\n");


	if (class != 'U')
		printf(" Comparison of RMS-norms of residual\n");
	else
		printf(" RMS-norms of residual\n");


	for (m = 0; m < 5; m++)
	{
		if (class == 'U')
			printf("          %d %20.13E", m + 1, xcr[m]);
		else if (!isnan(xcrdif[m]) && xcrdif[m] <= epsilon)
			printf("          %d %20.13E %20.13E %20.13E\n",
				   m + 1, xcr[m], xcrref[m], xcrdif[m]);
		else
		{
			verified = false;
			printf(" FAILURE: %d %20.13E %20.13E %20.13E\n",
				   m + 1, xcr[m], xcrref[m], xcrdif[m]);
		}
	}


	if (class != 'U')
		printf(" Comparison of RMS-norms of solution error\n");
	else
		printf(" RMS-norms of solution error\n");


	for (m = 0; m < 5; m++)
	{
		if (class == 'U')
			printf("          %d %20.13E", m + 1, xce[m]);
		else if (!isnan(xcedif[m]) && xcedif[m] <= epsilon)
			printf("          %d %20.13E %20.13E %20.13E\n",
				   m + 1, xce[m], xceref[m], xcedif[m]);
		else
		{
			verified = false;
			printf(" FAILURE: %d %20.13E %20.13E %20.13E\n",
				   m + 1, xce[m], xceref[m], xcedif[m]);
		}
	}


	if (class == 'U')
		printf(" No reference values provided\n No verification performed\n");
	else if (verified)
		printf(" Verification Successful\n");
	else
		printf(" Verification failed\n");

	*p_verified = verified;
	*p_class = class;

	return;
}


