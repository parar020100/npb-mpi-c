#include "sp_data_c.h"
#include <mpi.h>
#include "mpinpb_c.h"

//---------------------------------------------------------------------
// this function performs the solution of the approximate factorization
// step in the x-direction for all five matrix components
// simultaneously. The Thomas algorithm is employed to solve the
// systems for the x-lines. Boundary conditions are non-periodic
//---------------------------------------------------------------------
void x_solve_(void)
{

    int i, j, k, jp, kp, n, iend, jsize, ksize, i1, i2,
        buffer_size, c, m, p, istart, stage;

    MPI_Request requests[2];
    MPI_Status statuses[2];

    int k_from, k_to, j_from, j_to;

    double  r1, r2, d, e, s[5], sm1, sm2, fac1, fac2;

    //--------------------------------------------------------------------------
    // now do a sweep on a layer-by-layer basis, i.e. sweeping through cells
    // on this node in the direction of increasing i for the forward sweep, 
    // and after that reversing the direction for the backsubstitution
    //--------------------------------------------------------------------------

    if (timeron)
        timer_start(t_xsolve);

    //--------------------------------------------------------------------------
    //                          FORWARD ELIMINATION
    //--------------------------------------------------------------------------

    for (stage = 1; stage <= ncells; stage++)
    {
        c = SLICE(stage,1);

        istart = 0;
        iend  = CELL_SIZE(c,1) - 1;

        jsize = CELL_SIZE(c,2);
        ksize = CELL_SIZE(c,3);
        jp    = CELL_COORD(c,2) - 1;
        kp    = CELL_COORD(c,3) - 1;

        k_from = START(c, 3);
        k_to = ksize - END(c,3) - 1;
        j_from = START(c, 2);
        j_to = jsize - END(c,2) - 1;

        buffer_size = (jsize - START(c, 2) - END(c,2))
                    * (ksize - START(c, 3) - END(c,3));

        if (stage != 1)
        {
            //------------------------------------------------------------------
            //   if this is not the first processor in this row of cells, 
            //   receive data from predecessor containing the right hand
            //   sides and the upper diagonal elements of the previous two rows
            //------------------------------------------------------------------

            if (timeron)
                timer_start(t_xcomm);

            MPI_Irecv(&IN_BUFFER(1), 22 * buffer_size, dp_type, PREDECESSOR(1),
                      DEFAULT_TAG, comm_solve, &requests[0]);

            if (timeron)
                timer_stop(t_xcomm);

            //------------------------------------------------------------------
            //   communication has already been started.
            //   compute the left hand side while waiting for the msg
            //------------------------------------------------------------------

            lhsx_(&c);

            //------------------------------------------------------------------
            //   wait for pending communication to complete
            //   This waits on the current receive and on the send
            //   from the previous stage. They always come in pairs. 
            //------------------------------------------------------------------

            if (timeron)
                timer_start(t_xcomm);
            MPI_Waitall(2, requests, statuses);
            if (timeron)
                timer_stop(t_xcomm);

            //------------------------------------------------------------------
            //   unpack the buffer
            //------------------------------------------------------------------

            i = istart;
            i1 = istart + 1;
            n = 0;

            //------------------------------------------------------------------
            //   create a running pointer
            //------------------------------------------------------------------

            p = 0;
            for (k = k_from; k <= k_to; k++)
                for (j = j_from; j <= j_to; j++)
                {
                    LHS(c,n+2,k,j,i) -= (IN_BUFFER(p+1) * LHS(c,n+1,k,j,i));
                    LHS(c,n+3,k,j,i) -= (IN_BUFFER(p+2) * LHS(c,n+1,k,j,i));
                    for (m = 1; m <= 3; m++)
                        RHS(c,m,k,j,i) -= (IN_BUFFER(p+2+m) * LHS(c,n+1,k,j,i));

                    d = IN_BUFFER(p+6);
                    e = IN_BUFFER(p+7);

                    for (m = 1; m <= 3; m++)
                        s[m - 1] = IN_BUFFER(p+7+m);

                    r1 = LHS(c,n+2,k,j,i);
                    LHS(c,n+3,k,j,i) -= d * r1;
                    LHS(c,n+4,k,j,i) -= e * r1;

                    for (m = 1; m <= 3; m++)
                        RHS(c,m,k,j,i) -= s[m-1] * r1;

                    r2 = LHS(c,n+1,k,j,i1);
                    LHS(c,n+2,k,j,i1) -= d * r2;
                    LHS(c,n+3,k,j,i1) -= e * r2;

                    for (m = 1; m <= 3; m++)
                        RHS(c,m,k,j,i1) -= s[m-1] * r2;

                    p += 10;
                }


            for (m = 4; m <= 5; m++)
            {
                n = (m - 3) * 5;

                for (k = k_from; k <= k_to; k++)
                    for (j = j_from; j <= j_to; j++)
                    {
                        LHS(c,n+2,k,j,i) -= (IN_BUFFER(p+1) * LHS(c,n+1,k,j,i));
                        LHS(c,n+3,k,j,i) -= (IN_BUFFER(p+2) * LHS(c,n+1,k,j,i));
                        RHS(c,m,k,j,i) -= (IN_BUFFER(p+3) * LHS(c,n+1,k,j,i));

                        d = IN_BUFFER(p+4);
                        e = IN_BUFFER(p+5);
                        s[m - 1] = IN_BUFFER(p+6);

                        r1 = LHS(c,n+2,k,j,i);
                        LHS(c,n+3,k,j,i) -= d * r1;
                        LHS(c,n+4,k,j,i) -= e * r1;
                        RHS(c,m,k,j,i) -= s[m-1] * r1;

                        r2 = LHS(c,n+1,k,j,i1);
                        LHS(c,n+2,k,j,i1) -= d * r2;
                        LHS(c,n+3,k,j,i1) -= e * r2;
                        RHS(c,m,k,j,i1) -= s[m-1] * r2;

                        p += 6;
                    }
            }

        }
        else // if this IS the first cell, we still compute the lhs
            lhsx_(&c);

        //----------------------------------------------------------------------
        //   perform the Thomas algorithm; first, FORWARD ELIMINATION
        //----------------------------------------------------------------------

        n = 0;

        #pragma omp parallel default(shared) private(i,j,k,m,i1,i2,fac1,fac2)
        {

            #pragma omp for schedule(static)
            for (k = k_from; k <= k_to; k++)
                for (j = j_from; j <= j_to; j++)
                    for (i = istart; i <= iend - 2; i++)
                    {
                        i1 = i + 1;
                        i2 = i + 2;

                        fac1 = 1.0 / LHS(c,n+3,k,j,i);
                        LHS(c,n+4,k,j,i) *= fac1;
                        LHS(c,n+5,k,j,i) *= fac1;
                        for (m = 1; m <= 3; m++)
                            RHS(c,m,k,j,i) *= fac1;

                        LHS(c,n+3,k,j,i1) -= (LHS(c,n+2,k,j,i1) * LHS(c,n+4,k,j,i));
                        LHS(c,n+4,k,j,i1) -= (LHS(c,n+2,k,j,i1) * LHS(c,n+5,k,j,i));
                        for (m = 1; m <= 3; m++)
                            RHS(c,m,k,j,i1) -= (LHS(c,n+2,k,j,i1) * RHS(c,m,k,j,i));

                        LHS(c,n+2,k,j,i2) -= (LHS(c,n+1,k,j,i2) * LHS(c,n+4,k,j,i));
                        LHS(c,n+3,k,j,i2) -= (LHS(c,n+1,k,j,i2) * LHS(c,n+5,k,j,i));
                        for (m = 1; m <= 3; m++)
                            RHS(c,m,k,j,i2) -= (LHS(c,n+1,k,j,i2) * RHS(c,m,k,j,i));
                    }


            //----------------------------------------------------------------------
            //   The last two rows in this grid block are a bit different, 
            //   since they do not have two more rows available for the
            //   elimination of off-diagonal entries
            //----------------------------------------------------------------------

            i = iend - 1;
            i1 = iend;

            #pragma omp for schedule(static)
            for (k = k_from; k <= k_to; k++)
                for (j = j_from; j <= j_to; j++)
                {
                    fac1 = 1.0 / LHS(c,n+3,k,j,i);
                    LHS(c,n+4,k,j,i) *= fac1;
                    LHS(c,n+5,k,j,i) *= fac1;
                    for (m = 1; m <= 3; m++)
                        RHS(c,m,k,j,i) *= fac1;

                    LHS(c,n+3,k,j,i1) -= (LHS(c,n+2,k,j,i1) * LHS(c,n+4,k,j,i));
                    LHS(c,n+4,k,j,i1) -= (LHS(c,n+2,k,j,i1) * LHS(c,n+5,k,j,i));
                    for (m = 1; m <= 3; m++)
                        RHS(c,m,k,j,i1) -= (LHS(c,n+2,k,j,i1) * RHS(c,m,k,j,i));

                    //--------------------------------------------------------------
                    //   scale the last row immediately (some of this is
                    //   overkill in case this is the last cell)
                    //--------------------------------------------------------------

                    fac2 = 1.0 / LHS(c,n+3,k,j,i1);
                    LHS(c,n+4,k,j,i1) *= fac2;
                    LHS(c,n+5,k,j,i1) *= fac2;
                    for (m = 1; m <= 3; m++)
                        RHS(c,m,k,j,i1) *= fac2;
                }

        }

        //----------------------------------------------------------------------
        //         do the u+c and the u-c factors
        //----------------------------------------------------------------------

        for (m = 4; m <= 5; m++)
        #pragma omp parallel default(shared) private(i,j,k,i1,i2,fac1,fac2,n)
        {
            n = (m - 3) * 5;
            #pragma omp for schedule(static)
            for (k = k_from; k <= k_to; k++)
                for (j = j_from; j <= j_to; j++)
                    for(int i = istart; i <= iend - 2; i++)
                    {
                        i1 = i + 1;
                        i2 = i + 2;
                        fac1 = 1.0 / LHS(c,n+3,k,j,i);

                        LHS(c,n+4,k,j,i) *= fac1;
                        LHS(c,n+5,k,j,i) *= fac1;
                        RHS(c,m,k,j,i) *= fac1;

                        LHS(c,n+3,k,j,i1) -= LHS(c,n+2,k,j,i1) * LHS(c,n+4,k,j,i);
                        LHS(c,n+4,k,j,i1) -= LHS(c,n+2,k,j,i1) * LHS(c,n+5,k,j,i);
                        RHS(c,m,k,j,i1) -= LHS(c,n+2,k,j,i1) * RHS(c,m,k,j,i);

                        LHS(c,n+2,k,j,i2) -= LHS(c,n+1,k,j,i2) * LHS(c,n+4,k,j,i);
                        LHS(c,n+3,k,j,i2) -= LHS(c,n+1,k,j,i2) * LHS(c,n+5,k,j,i);
                        RHS(c,m,k,j,i2) -= LHS(c,n+1,k,j,i2) * RHS(c,m,k,j,i);
                    }

            //------------------------------------------------------------------
            //   And again the last two rows separately
            //------------------------------------------------------------------
            i = iend - 1;
            i1 = iend;

            #pragma omp for schedule(static)
            for (k = k_from; k <= k_to; k++)
                for (j = j_from; j <= j_to; j++)
                {
                    fac1 = 1.0 / LHS(c,n+3,k,j,i);
                    LHS(c,n+4,k,j,i) *= fac1;
                    LHS(c,n+5,k,j,i) *= fac1;
                    RHS(c,m,k,j,i) *= fac1;

                    LHS(c,n+3,k,j,i1) -= LHS(c,n+2,k,j,i1) * LHS(c,n+4,k,j,i);
                    LHS(c,n+4,k,j,i1) -= LHS(c,n+2,k,j,i1) * LHS(c,n+5,k,j,i);
                    RHS(c,m,k,j,i1) -= LHS(c,n+2,k,j,i1) * RHS(c,m,k,j,i);

                    //----------------------------------------------------------
                    //   Scale the last row immediately
                    //   (some of this is overkill if this is the last cell)
                    //----------------------------------------------------------

                    fac2 = 1.0 / LHS(c,n+3,k,j,i1);
                    LHS(c,n+4,k,j,i1) *= fac2;
                    LHS(c,n+5,k,j,i1) *= fac2;
                    RHS(c,m,k,j,i1) *= fac2;
                }
        }

        //----------------------------------------------------------------------
        //   send information to the next processor, except when this
        //   is the last grid block
        //----------------------------------------------------------------------

        if (stage != ncells)
        {
            //------------------------------------------------------------------
            //   create a running pointer for the send buffer
            //------------------------------------------------------------------
            p = 0;
            n = 0;
            for (k = k_from; k <= k_to; k++)
                for (j = j_from; j <= j_to; j++)
                    for (i = iend - 1; i <= iend; i++)
                    {
                        OUT_BUFFER(p+1) = LHS(c,n+4,k,j,i);
                        OUT_BUFFER(p+2) = LHS(c,n+5,k,j,i);
                        for (m = 1; m <= 3; m++)
                            OUT_BUFFER(p+m+2) = RHS(c,m,k,j,i);
                        p += 5;
                    }

            for (m = 4; m <= 5; m++)
            {
                n = (m - 3) * 5;
                for (k = k_from; k <= k_to; k++)
                    for (j = j_from; j <= j_to; j++)
                        for(i = iend - 1; i <= iend; i++)
                        {
                            OUT_BUFFER(p+1) = LHS(c,n+4,k,j,i);
                            OUT_BUFFER(p+2) = LHS(c,n+5,k,j,i);
                            OUT_BUFFER(p+3) = RHS(c,m,k,j,i);
                            p += 3;
                        }
            }

            //---------------------------------------------------------------------
            // send data to next phase
            // can't receive data yet because buffer size will be wrong 
            //---------------------------------------------------------------------

            if (timeron)
                timer_start(t_xcomm);

            MPI_Isend(&OUT_BUFFER(1), 22 * buffer_size, dp_type, SUCCESSOR(1),
                      DEFAULT_TAG, comm_solve, &requests[1]);

            if (timeron)
                timer_stop(t_xcomm);
        }
    }


    //--------------------------------------------------------------------------
    //   now go in the reverse direction
    //--------------------------------------------------------------------------

    //--------------------------------------------------------------------------
    //                              BACKSUBSTITUTION
    //--------------------------------------------------------------------------

    for (stage = ncells; stage >= 1; stage--)
    {
        c = SLICE(stage,1);

        istart = 0;
        iend = CELL_SIZE(c,1) - 1;

        jsize = CELL_SIZE(c,2);
        ksize = CELL_SIZE(c,3);
        jp    = CELL_COORD(c,2) - 1;
        kp    = CELL_COORD(c,3) - 1;

        k_from = START(c, 3);
        k_to = ksize - END(c,3) - 1;
        j_from = START(c, 2);
        j_to = jsize - END(c,2) - 1;

        buffer_size = (jsize - START(c, 2) - END(c,2))
                    * (ksize - START(c, 3) - END(c,3));

        if (stage != ncells)
        {
            //------------------------------------------------------------------
            //   if this is not the starting cell in this row of cells,
            //   wait for a message to be received, containing the
            //   solution of the previous two stations
            //------------------------------------------------------------------

            if (timeron)
                timer_start(t_xcomm);

            MPI_Irecv(&IN_BUFFER(1), 10 * buffer_size, dp_type, SUCCESSOR(1),
                      DEFAULT_TAG, comm_solve, &requests[0]);

            if (timeron)
                timer_stop(t_xcomm);

            //------------------------------------------------------------------
            //   communication has already been started
            //   while waiting, do the  block-diagonal inversion for the
            //   cell that was just finished
            //------------------------------------------------------------------
            ninvr_(&SLICE(stage+1,1));

            //------------------------------------------------------------------
            //   wait for pending communication to complete
            //------------------------------------------------------------------

            if (timeron)
                timer_start(t_xcomm);

            MPI_Waitall(2, requests, statuses);

            if (timeron)
                timer_stop(t_xcomm);


            //------------------------------------------------------------------
            //   unpack the buffer for the first three factors
            //------------------------------------------------------------------

            n = 0;
            p = 0;
            i = iend;
            i1 = i - 1;
            for (m = 1; m <= 3; m++)
                for (k = k_from; k <= k_to; k++)
                    for (j = j_from; j <= j_to; j++)
                    {
                        sm1 = IN_BUFFER(p+1);
                        sm2 = IN_BUFFER(p+2);
                        RHS(c,m,k,j,i)  -= (LHS(c,n+4,k,j,i)  * sm1    + LHS(c,n+5,k,j,i)  * sm2);
                        RHS(c,m,k,j,i1) -= (LHS(c,n+4,k,j,i1) * RHS(c,m,k,j,i) + LHS(c,n+5,k,j,i1) * sm1);
                        p += 2;
                    }

            //------------------------------------------------------------------
            //   now unpack the buffer for the remaining two factors
            //------------------------------------------------------------------

            for (m = 4; m <= 5; m++)
            {
                n = (m - 3) * 5;
                for (k = k_from; k <= k_to; k++)
                    for (j = j_from; j <= j_to; j++)
                    {
                        sm1 = IN_BUFFER(p+1);
                        sm2 = IN_BUFFER(p+2);
                        RHS(c,m,k,j,i)  -= (LHS(c,n+4,k,j,i)  * sm1    + LHS(c,n+5,k,j,i)  * sm2);
                        RHS(c,m,k,j,i1) -= (LHS(c,n+4,k,j,i1) * RHS(c,m,k,j,i) + LHS(c,n+5,k,j,i1) * sm1);
                        p += 2;
                    }
            }
        }
        else
        #pragma omp parallel default(shared) private(i,j,k,i1,n,m)
        {

            //------------------------------------------------------------------
            //   now we know this is the first grid block on the back sweep,
            //   so we don't need a message to start the substitution.
            //------------------------------------------------------------------

            i = iend - 1;
            i1 = iend;
            n = 0;

            for (m = 1; m <= 3; m++)
                #pragma omp for schedule(static)
                for (k = k_from; k <= k_to; k++)
                    for (j = j_from; j <= j_to; j++)
                        RHS(c,m,k,j,i) -= LHS(c,n+4,k,j,i) * RHS(c,m,k,j,i1);

            for (m = 4; m <= 5; m++)
            {
                n = (m - 3) * 5;
                #pragma omp for schedule(static)
                for (k = k_from; k <= k_to; k++)
                    for (j = j_from; j <= j_to; j++)
                        RHS(c,m,k,j,i) -= LHS(c,n+4,k,j,i) * RHS(c,m,k,j,i1);
            }
        }

        //----------------------------------------------------------------------
        //   Whether or not this is the last processor, we always have
        //   to complete the back-substitution 
        //----------------------------------------------------------------------

        //----------------------------------------------------------------------
        //   The first three factors
        //----------------------------------------------------------------------

        #pragma omp parallel default(shared) private(i,j,k,i1,i2,m,n)
        {

            n = 0;
            for (m = 1; m <= 3; m++)
                #pragma omp for schedule(static)
                for (k = k_from; k <= k_to; k++)
                    for (j = j_from; j <= j_to; j++)
                        for (i = iend - 2; i >= istart; i--)
                        {
                            i1 = i + 1;
                            i2 = i + 2;
                            RHS(c,m,k,j,i) -= (LHS(c,n+4,k,j,i) * RHS(c,m,k,j,i1) + LHS(c,n+5,k,j,i) * RHS(c,m,k,j,i2));
                        }

            //----------------------------------------------------------------------
            //   And the remaining two
            //----------------------------------------------------------------------

            for (m = 4; m <= 5; m++)
            {
                n = (m - 3) * 5;
                #pragma omp for schedule(static)
                for (k = k_from; k <= k_to; k++)
                    for (j = j_from; j <= j_to; j++)
                        for(i = iend - 2; i >= istart; i--)
                        {
                            i1 = i + 1;
                            i2 = i + 2;
                            RHS(c,m,k,j,i) -= (LHS(c,n+4,k,j,i) * RHS(c,m,k,j,i1) + LHS(c,n+5,k,j,i) * RHS(c,m,k,j,i2));
                        }
            }

        }

        //----------------------------------------------------------------------
        //   send on information to the previous processor, if needed
        //----------------------------------------------------------------------

        if (stage != 1)
        {
            i = istart;
            i1 = istart + 1;
            p = 0;
            for (m = 1; m <= 5; m++)
                for (k = k_from; k <= k_to; k++)
                    for (j = j_from; j <= j_to; j++)
                    {
                        OUT_BUFFER(p+1) = RHS(c,m,k,j,i);
                        OUT_BUFFER(p+2) = RHS(c,m,k,j,i1);
                        p += 2;
                    }

            //------------------------------------------------------------------
            //   pack and send the buffer
            //------------------------------------------------------------------

            if(timeron)
                timer_start(t_xcomm);

            MPI_Isend(&OUT_BUFFER(1), 10 * buffer_size, dp_type, PREDECESSOR(1),
                      DEFAULT_TAG, comm_solve, &requests[1]);

            if(timeron)
                timer_stop(t_xcomm);

        }

        //----------------------------------------------------------------------
        //   If this was the last stage, do the block-diagonal inversion
        //----------------------------------------------------------------------

        if (stage == 1)
            ninvr_(&c);
    }

    if (timeron)
        timer_stop(t_xsolve);

    return;
}
