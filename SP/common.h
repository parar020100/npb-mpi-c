// Common declarations

#ifndef COMMON_H
#define COMMON_H

#include "lang_mode.h"

typedef enum { false, true } logical;

#define min(x,y)	((x) < (y) ? (x) : (y))
#define max(x,y)	((x) > (y) ? (x) : (y))

// Functions
extern void copy_faces_(void);
extern void compute_rhs_(void);
extern void txinvr_(void);
extern void tzetar_(int*);
extern void ninvr_(int *);
extern void pinvr_(int *);
extern void x_solve_(void);
extern void y_solve_(void);
extern void z_solve_(void);
extern void add_(void);
extern void adi_(void);
extern void alloc_space_(void);
extern void exact_rhs_(void);
extern void make_set_(void);
extern void initialize_(void);
extern void lhsinit_(void);
extern void set_constants_(void);
extern void compute_buffer_size_(int*);
extern void verify_(char*, logical*);
extern void exact_solution_(double *, double *, double *, double[5]);
extern void lhsx_(int*);
extern void lhsy_(int*);
extern void lhsz_(int*);
extern void rhs_norm_(double *);
extern void error_norm_(double *);
extern void set_class_(int*, char*);

/* timers.h contents */
#ifdef FORTRAN_INTERACTION
void timer_clear_(int n);
void timer_start_(int n);
void timer_stop_(int n);
double timer_read_(int n);

#define timer_clear(X) timer_clear_(X)
#define timer_start(X) timer_start_(X)
#define timer_stop(X) timer_stop_(X)
#define timer_read(X) timer_read_(X)

#else
#include "../common/c_timers.h"

void c_print_results(char*, char, int, int, int, int, int, int, double, double, char*,
					 int, char*, char*, char*, char*, char *, char *, char *, char *);
#endif

// Timer constants
#define t_total		1
#define t_rhs		2
#define t_xsolve	3
#define t_ysolve	4
#define t_zsolve	5
#define t_bpack		6
#define t_exch		7
#define t_xcomm		8
#define t_ycomm		9
#define t_zcomm		10
#define t_last		10

#endif //COMMON_H