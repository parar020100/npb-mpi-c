#include "dynamic_array.h"
#include <mpi.h>

#define SZ sizeof(void *)

static void **mem_ptrs;
static int mem_ptrs_count = 0, mem_ptrs_size = 1<<10;

static void save_mem_ptr(void *p)
{
    if (!mem_ptrs || mem_ptrs_count > mem_ptrs_size)
    {
        mem_ptrs_size <<= 1;
        mem_ptrs = realloc(mem_ptrs, mem_ptrs_size * SZ);
    }

    mem_ptrs[mem_ptrs_count++] = p;
}

void free_dynamic_memory(void)
{
    for (int i = 0; i < mem_ptrs_count; i++)
        free(mem_ptrs[i]);
    free(mem_ptrs);
}

static void stop_all(void)
{
    free_dynamic_memory();
    fflush(stdout);
    MPI_Abort(MPI_COMM_WORLD, MPI_ERR_OTHER);
    exit(1);
}

static void *calloc_safe(int num, size_t sz)
{
    if (num < 0 || sz < 0)
        goto allocation_error;

    void *p = calloc((size_t) num, sz);

    if (p == NULL)
        goto allocation_error;

    save_mem_ptr(p);
    return p;

allocation_error:
    printf("Error encountered in allocating space:\n");
    printf("    Failed to allocate %dx%ld bytes\n", num, sz);
    stop_all();
}

static darray darray_allocate_pointers(int n, int d[5])
{
    darray p;

    if (n > 1)
        p.v1 = calloc_safe(d[0], SZ);

    if (n > 2)
        for (int i = 0; i < d[0]; i++)
            p.v2[i] = calloc_safe(d[1], SZ);

    if (n > 3)
        for (int i = 0; i < d[0]; i++)
            for (int j = 0; j < d[1]; j++)
                p.v3[i][j] = calloc_safe(d[2], SZ);

    if (n > 4)
        for (int i = 0; i < d[0]; i++)
            for (int j = 0; j < d[1]; j++)
                for (int k = 0; k < d[2]; k++)
                    p.v4[i][j][k] = calloc_safe(d[3], SZ);

    return p;
}


darray darray_map(void *src, size_t el_sz, int n, int d[5])
{
    darray p = darray_allocate_pointers(n, d);

    switch (n)
    {
        case 1:
            p.v1 = src;
            return p;
        case 2:
            for (int i = 0; i < d[0]; i++)
                p.v2[i] = src + i * d[1] * el_sz;
            return p;
        case 3:
            for (int i = 0; i < d[0]; i++)
                for (int j = 0; j < d[1]; j++)
                    p.v3[i][j] = src + (i * d[1] + j) * d[2] * el_sz;
            return p;
        case 4:
            for (int i = 0; i < d[0]; i++)
                for (int j = 0; j < d[1]; j++)
                    for (int k = 0; k < d[2]; k++)
                        p.v4[i][j][k] = src +
                            ((i * d[1] + j) * d[2] + k) * d[3] * el_sz;
            return p;
        case 5:
            for (int i = 0; i < d[0]; i++)
                for (int j = 0; j < d[1]; j++)
                    for (int k = 0; k < d[2]; k++)
                        for (int t = 0; t < d[3]; t++)
                            p.v5[i][j][k][t] = src +
                                (((i * d[1] + j) * d[2] + k) * d[3] + t) * d[4] * el_sz;

            return p;

        default:
            printf("Error encountered in mapping dynamic memory\n");
            stop_all();
    }
}

darray darray_create(size_t el_sz, int n, int d[5])
{
    darray p = darray_allocate_pointers(n, d);

    switch (n)
    {
        case 1:
            p.v1 = calloc_safe(el_sz, d[0]);
            return p;
        case 2:
            for (int i = 0; i < d[0]; i++)
                p.v2[i] = calloc_safe(el_sz, d[1]);
            return p;
        case 3:
            for (int i = 0; i < d[0]; i++)
                for (int j = 0; j < d[1]; j++)
                    p.v3[i][j] = calloc_safe(el_sz, d[2]);
            return p;
        case 4:
            for (int i = 0; i < d[0]; i++)
                for (int j = 0; j < d[1]; j++)
                    for (int k = 0; k < d[2]; k++)
                        p.v4[i][j][k] = calloc_safe(el_sz, d[3]);
            return p;
        case 5:
            for (int i = 0; i < d[0]; i++)
                for (int j = 0; j < d[1]; j++)
                    for (int k = 0; k < d[2]; k++)
                        for (int t = 0; t < d[3]; t++)
                            p.v5[i][j][k][t] = calloc_safe(el_sz, d[4]);
            return p;
        default:
            printf("Error encountered in allocating dynamic memory\n");
            stop_all();
    }
}


darray darray_setup(void *src, size_t el_sz, size_t dimentions,
                    int dim_min_0, int dim_max_0,
                    int dim_min_1, int dim_max_1,
                    int dim_min_2, int dim_max_2,
                    int dim_min_3, int dim_max_3,
                    int dim_min_4, int dim_max_4)
{
    int d[5];

    d[0] = dim_max_0 - dim_min_0 + 1;
    d[1] = dim_max_1 - dim_min_1 + 1;
    d[2] = dim_max_2 - dim_min_2 + 1;
    d[3] = dim_max_3 - dim_min_3 + 1;
    d[4] = dim_max_4 - dim_min_4 + 1;

    if (src)
        return darray_map(src, el_sz, dimentions, d);

    return darray_create(el_sz, dimentions, d);
}
