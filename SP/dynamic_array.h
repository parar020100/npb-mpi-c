#ifndef dynamic_array_H
#define dynamic_array_H

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

//typedef void* darray;
typedef union darray
{
    void *v1;
    void **v2;
    void ***v3;
    void ****v4;
    void *****v5;
    int *i1;
    int **i2;
    int ***i3;
    int ****i4;
    int *****i5;
    double *d1;
    double **d2;
    double ***d3;
    double ****d4;
    double *****d5;
} darray;

void free_dynamic_memory(void);

darray darray_setup(void *src, size_t el_sz, size_t dimentions,
                           int dim_min_0, int dim_max_0,
                           int dim_min_1, int dim_max_1,
                           int dim_min_2, int dim_max_2,
                           int dim_min_3, int dim_max_3,
                           int dim_min_4, int dim_max_4);

#define SETUP_DARR_5(p, el, m1,M1, m2,M2, m3,M3, m4,M4, m5,M5) \
    darray_setup (p, el, 5, m1,M1, m2,M2, m3,M3, m4,M4, m5,M5)

#define SETUP_DARR_4(p, el, m1,M1, m2,M2, m3,M3, m4,M4) \
    darray_setup (p, el, 4, m1,M1, m2,M2, m3,M3, m4,M4,  0,0 )

#define SETUP_DARR_3(p, el, m1,M1, m2,M2, m3,M3) \
    darray_setup (p, el, 3, m1,M1, m2,M2, m3,M3,  0,0,   0,0 )

#define SETUP_DARR_2(p, el, m1,M1, m2,M2) \
    darray_setup (p, el, 2, m1,M1, m2,M2,  0,0,   0,0,   0,0 )

#define SETUP_DARR_1(p, el, m1,M1) \
    darray_setup (p, el, 1, m1,M1,  0,0,   0,0,   0,0,   0,0 )


#endif //dynamic_array_H