#include "common.h"
#include <mpi.h>
#include "lang_mode.h"

#ifndef MPINPB_C_H
#define MPINPB_C_H

#ifdef FORTRAN_INTERACTION
extern struct {
    int node, no_nodes, total_nodes, root,
        comm_setup, comm_solve, comm_rhs, dp_type;

    logical active;
} mpinpb_fortran_vars_;

#define M mpinpb_fortran_vars_

// int
#define node         M.node
#define no_nodes     M.no_nodes
#define total_nodes  M.total_nodes
#define mroot        M.root
#define comm_setup   MPI_Comm_f2c(M.comm_setup)
#define comm_solve   MPI_Comm_f2c(M.comm_solve)
#define comm_rhs     MPI_Comm_f2c(M.comm_rhs)
#define dp_type      MPI_Type_f2c(M.dp_type)

// logical
#define active      M.active

#else //FORTRAN_INTERACTION

extern int node, no_nodes, total_nodes, mroot;

extern MPI_Comm comm_setup, comm_solve, comm_rhs;
extern MPI_Datatype dp_type;

extern logical active;

#endif //FORTRAN_INTERACTION

#define DEFAULT_TAG 0

#endif //MPINPB_C_H