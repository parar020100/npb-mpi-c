#include "sp_data_c.h"
#include <mpi.h>
#include "mpinpb_c.h"

int node, no_nodes, total_nodes, mroot;
MPI_Comm comm_setup, comm_solve, comm_rhs;
MPI_Datatype dp_type;
logical active;

extern void get_active_nprocs(int pkind, int *p_np1, int *p_np2, int *p_npa,
							  int *p_nprocs, int *p_rank, MPI_Comm *comm_out,
							  logical *p_active);

//---------------------------------------------------------------------
// set up MPI stuff
//---------------------------------------------------------------------

void setup_mpi(void)
{
	int nc, color;

	MPI_Init(NULL, NULL);

	if (!CONVERTDOUBLE)
		dp_type = MPI_DOUBLE_PRECISION;
	else
		dp_type = MPI_REAL;

	//---------------------------------------------------------------------
	//	 get a process grid that requires a square number of procs.
	//	 excess ranks are marked as inactive.
	//---------------------------------------------------------------------
	get_active_nprocs(1, &nc, &maxcells, &no_nodes, &total_nodes, &node,
					  &comm_setup, &active);

	if (! active)
		return;

	MPI_Comm_dup(comm_setup, &comm_solve);
	MPI_Comm_dup(comm_setup, &comm_rhs);

	//---------------------------------------------------------------------
	//	 let node 0 be the root for the group (there is only one)
	//---------------------------------------------------------------------
	mroot = 0;

	return;
}

