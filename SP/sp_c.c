#include "sp_data_c.h"
#include "mpinpb_c.h"
#include <mpi.h>

void setup_mpi(void);

int main(void)
{

	int i, niter, step, c, fstatus;
	double mflops, n3, t, tmax;
	logical verified;
	double tsum[t_last+2], t1[t_last+2], tming[t_last+2], tmaxg[t_last+2];
	char class;
	FILE *fp;
	const char * t_recs[t_last+2] = {"total", "rhs", "xsolve", "ysolve", "zsolve",
		"bpack", "exch", "xcomm", "ycomm", "zcomm", " totcomp", " totcomm"};

	setup_mpi();

	if (! active)
		goto end;

	//---------------------------------------------------------------------
	//      Root node reads input file (if it exists) else takes
	//      defaults from parameters
	//---------------------------------------------------------------------

	if (node == mroot)
	{
		printf("\n\n NAS Parallel Benchmarks 3.4 -- SP Benchmark\n");

		check_timer_flag();

		if ((fp = fopen("inputsp.data", "r")) != NULL) {
			int result;
			printf(" Reading from input file inputsp.data\n");
			result = fscanf(fp, "%d", &niter);
			while (fgetc(fp) != '\n') {}
			result = fscanf(fp, "%lf", &dt);
			while (fgetc(fp) != '\n') {}
			result = fscanf(fp, "%d%d%d", &GRID_POINTS(1), &GRID_POINTS(2), &GRID_POINTS(3));
			fclose(fp);
		} else {
			printf(" No input file inputsp.data. Using compiled defaults\n");
			niter = NITER_DEFAULT;
			dt    = DT_DEFAULT;
			GRID_POINTS(1) = PROBLEM_SIZE;
			GRID_POINTS(2) = PROBLEM_SIZE;
			GRID_POINTS(3) = PROBLEM_SIZE;
		}

		set_class_(&niter, &class);

		printf(" Size: %4dx%4dx%4d (class %c)\n",
			   GRID_POINTS(1), GRID_POINTS(2), GRID_POINTS(3), class);
		printf(" Iterations: %4d    dt:  %11.7f\n", niter, dt);
		printf(" Total number of processes: %6d\n", total_nodes);

		if (no_nodes != total_nodes)
			printf(" WARNING: Number of processes is not a square number (%d active)\n",
				   no_nodes);

		printf("\n");

	}

	MPI_Bcast(&niter, 1, MPI_INTEGER, mroot, comm_setup);
	MPI_Bcast(&dt, 1, dp_type, mroot, comm_setup);
	MPI_Bcast(&GRID_POINTS(1), 3, MPI_INTEGER, mroot, comm_setup);
	MPI_Bcast(&timeron, 1, MPI_LOGICAL, mroot, comm_setup);


	alloc_space_();
	make_set_();

	for (c = 1; c <= ncells; c++)
		if (CELL_SIZE(c,1) > IMAX || CELL_SIZE(c,2) > JMAX || CELL_SIZE(c,3) > KMAX)
		{
			printf("%d %d %d %d %d\n", node, c, CELL_SIZE(c, 1), CELL_SIZE(c, 2), CELL_SIZE(c, 3));
			printf(" Problem size too big for compiled array sizes\n");
			goto end;
		}

	for (i = 1; i <= t_last; i++)
		timer_clear(i);

	set_constants_();
	initialize_();
	lhsinit_();
	exact_rhs_();

	int dim = 5;
	compute_buffer_size_(&dim);

	//---------------------------------------------------------------------
	//      do one time step to touch all code, and reinitialize
	//---------------------------------------------------------------------
	adi_();
	initialize_();

	//---------------------------------------------------------------------
	//      Synchronize before placing time stamp
	//---------------------------------------------------------------------

	for (i = 1; i <= t_last; i++)
		timer_clear(i);

	MPI_Barrier(comm_setup);

	timer_clear(1);
	timer_start(1);

	for (step = 1; step <= niter; step++)
	{
		if (node == mroot && ((step % 20) == 0 || step == 1))
			printf(" Time step %4d\n", step);

		adi_();
	}

	timer_stop(1);
	t = timer_read(1);

	verify_(&class, &verified);

	MPI_Reduce(&t, &tmax, 1, dp_type, MPI_MAX, mroot, comm_setup);

	if( node == mroot )
	{
		if( tmax != 0.0 )
		{
			n3 = ((double) GRID_POINTS(1)) * GRID_POINTS(2) * GRID_POINTS(3);
			t = (GRID_POINTS(1) + GRID_POINTS(2) + GRID_POINTS(3)) / 3.0;
			mflops = 1.0E-6 * ((double) niter) *
				(881.174 * n3 - 4683.91 * t * t + 11484.5 * t - 19272.4) / tmax;
		}
		else
			mflops = 0.0;

		c_print_results("SP", class, GRID_POINTS(1), GRID_POINTS(2), GRID_POINTS(3),
						niter, no_nodes, total_nodes, tmax, mflops, "floating point",
						verified, NPBVERSION, COMPILETIME, CS8, CS9, CS10, CS11, CS12, CS13);
	}

	if (! timeron)
		goto end;

	for (i = 0; i < t_last; i++)
		t1[i] = timer_read(i + 1);

	t1[t_ysolve-1] 	= t1[t_ysolve-1] - t1[t_ycomm-1];
	t1[t_xsolve-1] 	= t1[t_xsolve-1] - t1[t_xcomm-1];
	t1[t_zsolve-1] 	= t1[t_zsolve-1] - t1[t_zcomm-1];
	t1[t_last+1] 	= t1[t_xcomm-1]  + t1[t_ycomm-1] + t1[t_zcomm-1] + t1[t_exch-1];
	t1[t_last] 		= t1[t_total-1]  - t1[t_last+1];

	MPI_Reduce(&t1, &tsum,  t_last + 2, dp_type, MPI_SUM, 0, comm_setup);
	MPI_Reduce(&t1, &tming, t_last + 2, dp_type, MPI_MIN, 0, comm_setup);
	MPI_Reduce(&t1, &tmaxg, t_last + 2, dp_type, MPI_MAX, 0, comm_setup);

	if (node == 0)
	{
		printf(" nprocs = %6i           minimum     maximum     average\n", no_nodes);
		for (i = 0; i < t_last + 2; i++)
		{
			tsum[i] /= no_nodes;
			printf(" timer %2i(%8s) :  %10.4f  %10.4f  %10.4f\n",
				   i, t_recs[i], tming[i], tmaxg[i], tsum[i]);
		}
	}

end:
	free_dynamic_memory();
	fflush(stdout);
	MPI_Barrier(MPI_COMM_WORLD);
	MPI_Finalize();
}
