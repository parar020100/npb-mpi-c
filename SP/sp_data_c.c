#include "sp_data_c.h"
#include "dynamic_array.h"
#include "lang_mode.h"

#define SZ_INT sizeof(int)
#define SZ_DBL sizeof(double)

//dynamic arrays

// int
darray cell_coord, cell_low, cell_high, cell_size, start, end, slice;

// double
darray u, us, vs, ws, qs, ainv, rho_i, speed, square, rhs, forcing, lhs;
darray in_buffer, out_buffer;
darray cv, rhon, rhos, rhoq, cuf, q, ue, buf;

void init_dynamic_c_arrays_(void **p_cell_coord,	void **p_cell_low,
							void **p_cell_high,		void **p_cell_size,
							void **p_start,			void **p_end,
							void **p_slice,			void **p_u,
							void **p_us,			void **p_vs,
							void **p_ws,			void **p_qs,
							void **p_ainv,			void **p_rho_i,
							void **p_speed,			void **p_square,
							void **p_rhs,			void **p_forcing,
							void **p_lhs,			void **p_in_buffer,
							void **p_out_buffer,	void **p_cv,
							void **p_rhon,			void **p_rhos,
							void **p_rhoq,			void **p_cuf,
							void **p_q,				void **p_ue,
							void **p_buf)
{
	in_buffer	= SETUP_DARR_1( *p_in_buffer,   SZ_DBL,     1,BUF_SIZE         );
	out_buffer	= SETUP_DARR_1( *p_out_buffer,  SZ_DBL,     1,BUF_SIZE         );
	cv			= SETUP_DARR_1( *p_cv,          SZ_DBL,    -2,MAX_CELL_DIM + 1 );
	rhon		= SETUP_DARR_1( *p_rhon,        SZ_DBL,    -2,MAX_CELL_DIM + 1 );
	rhos		= SETUP_DARR_1( *p_rhos,        SZ_DBL,    -2,MAX_CELL_DIM + 1 );
	rhoq		= SETUP_DARR_1( *p_rhoq,        SZ_DBL,    -2,MAX_CELL_DIM + 1 );
	cuf			= SETUP_DARR_1( *p_cuf,         SZ_DBL,    -2,MAX_CELL_DIM + 1 );
	q			= SETUP_DARR_1( *p_q,           SZ_DBL,    -2,MAX_CELL_DIM + 1 );

	cell_coord	= SETUP_DARR_2( *p_cell_coord,  SZ_INT,    1,maxcells,  1,3                );
	cell_low	= SETUP_DARR_2( *p_cell_low,    SZ_INT,    1,maxcells,  1,3                );
	cell_high	= SETUP_DARR_2( *p_cell_high,   SZ_INT,    1,maxcells,  1,3                );
	cell_size	= SETUP_DARR_2( *p_cell_size,   SZ_INT,    1,maxcells,  1,3                );
	start		= SETUP_DARR_2( *p_start,       SZ_INT,    1,maxcells,  1,3                );
	end			= SETUP_DARR_2( *p_end,         SZ_INT,    1,maxcells,  1,3                );
	slice		= SETUP_DARR_2( *p_slice,       SZ_INT,    1,maxcells,  1,3                );
	ue			= SETUP_DARR_2( *p_ue,          SZ_DBL,    1,5,        -2,MAX_CELL_DIM + 1 );
	buf			= SETUP_DARR_2( *p_buf,         SZ_DBL,    1,5,        -2,MAX_CELL_DIM + 1 );

	us			= SETUP_DARR_4( *p_us,          SZ_DBL,    1,maxcells,  -1,KMAX,  -1,JMAX,  -1,IMAX);
	vs			= SETUP_DARR_4( *p_vs,          SZ_DBL,    1,maxcells,  -1,KMAX,  -1,JMAX,  -1,IMAX);
	ws			= SETUP_DARR_4( *p_ws,          SZ_DBL,    1,maxcells,  -1,KMAX,  -1,JMAX,  -1,IMAX);
	qs			= SETUP_DARR_4( *p_qs,          SZ_DBL,    1,maxcells,  -1,KMAX,  -1,JMAX,  -1,IMAX);
	ainv		= SETUP_DARR_4( *p_ainv,        SZ_DBL,    1,maxcells,  -1,KMAX,  -1,JMAX,  -1,IMAX);
	rho_i		= SETUP_DARR_4( *p_rho_i,       SZ_DBL,    1,maxcells,  -1,KMAX,  -1,JMAX,  -1,IMAX);
	speed		= SETUP_DARR_4( *p_speed,       SZ_DBL,    1,maxcells,  -1,KMAX,  -1,JMAX,  -1,IMAX);
	square		= SETUP_DARR_4( *p_square,      SZ_DBL,    1,maxcells,  -1,KMAX,  -1,JMAX,  -1,IMAX);

	u			= SETUP_DARR_5( *p_u,           SZ_DBL,    1,maxcells,  1,5,  -2,KMAX+1,  -2,JMAXP+1, -2,IMAXP+1 );
	rhs			= SETUP_DARR_5( *p_rhs,         SZ_DBL,    1,maxcells,  1,5,   0,KMAX-1,   0,JMAXP-1,  0,IMAXP-1 );
	forcing		= SETUP_DARR_5( *p_forcing,     SZ_DBL,    1,maxcells,  1,5,   0,KMAX-1,   0,JMAXP-1,  0,IMAXP-1 );
	lhs			= SETUP_DARR_5( *p_lhs,         SZ_DBL,    1,maxcells,  1,15,  0,KMAX-1,   0,JMAXP-1,  0,IMAXP-1 );

}

#ifndef FORTRAN_INTERACTION

int ncells;
int grid_points[3];

double tx1, tx2, tx3, ty1, ty2, ty3, tz1, tz2, tz3,
	   dx1, dx2, dx3, dx4, dx5, dy1, dy2, dy3, dy4,
	   dy5, dz1, dz2, dz3, dz4, dz5, dssp, dt,
	   ce[13][5], dxmax, dymax, dzmax, xxcon1, xxcon2,
	   xxcon3, xxcon4, xxcon5, dx1tx1, dx2tx1, dx3tx1,
	   dx4tx1, dx5tx1, yycon1, yycon2, yycon3, yycon4,
	   yycon5, dy1ty1, dy2ty1, dy3ty1, dy4ty1, dy5ty1,
	   zzcon1, zzcon2, zzcon3, zzcon4, zzcon5, dz1tz1,
	   dz2tz1, dz3tz1, dz4tz1, dz5tz1, dnxm1, dnym1,
	   dnzm1, c1c2, c1c5, c3c4, c1345, conz1, c1, c2,
	   c3, c4, c5, c4dssp, c5dssp, dtdssp, dttx1, bt,
	   dttx2, dtty1, dtty2, dttz1, dttz2, c2dttx1,
	   c2dtty1, c2dttz1, comz1, comz4, comz5, comz6,
	   c3c4tx3, c3c4ty3, c3c4tz3, c2iv, con43, con16;

int maxcells, IMAX, JMAX, KMAX, MAX_CELL_DIM, BUF_SIZE, IMAXP, JMAXP;

int predecessor[3], successor[3], grid_size[3];

int west_size, east_size, bottom_size, top_size,
	north_size, south_size, start_send_west,
	start_send_east, start_send_south, start_send_north,
	start_send_bottom, start_send_top, start_recv_west,
	start_recv_east, start_recv_south, start_recv_north,
	start_recv_bottom, start_recv_top;

logical timeron;

void alloc_space_(void)
{
	void *n = NULL;

	MAX_CELL_DIM = (PROBLEM_SIZE / maxcells) + 1;

	IMAX = MAX_CELL_DIM;
	JMAX = MAX_CELL_DIM;
	KMAX = MAX_CELL_DIM;

	IMAXP = IMAX / 2 * 2 + 1;
	JMAXP = JMAX / 2 * 2 + 1;

	BUF_SIZE = MAX_CELL_DIM * MAX_CELL_DIM * (maxcells - 1) * 60 * 2 + 1;

	init_dynamic_c_arrays_(&n, &n, &n, &n, &n, &n, &n, &n, &n, &n, &n, &n, &n, &n, &n, &n, &n, &n,
						   &n, &n, &n, &n, &n, &n, &n, &n, &n, &n, &n);


}

#endif // FORTRAN_INTERACTION