#ifndef SP_DATA_C_H
#define SP_DATA_C_H

#include "common.h"
#include "npbparams_c.h"
#include "dynamic_array.h"

// Constants
#define EAST	2000
#define WEST	3000
#define NORTH	4000
#define SOUTH	5000
#define BOTTOM	6000
#define TOP		7000

#ifdef FORTRAN_INTERACTION

// Fortran variables

extern struct {
	int ncells;
	int grid_points[3];

	double tx1, tx2, tx3, ty1, ty2, ty3, tz1, tz2, tz3,
		   dx1, dx2, dx3, dx4, dx5, dy1, dy2, dy3, dy4,
		   dy5, dz1, dz2, dz3, dz4, dz5, dssp, dt,
		   ce[13][5], dxmax, dymax, dzmax, xxcon1, xxcon2,
		   xxcon3, xxcon4, xxcon5, dx1tx1, dx2tx1, dx3tx1,
		   dx4tx1, dx5tx1, yycon1, yycon2, yycon3, yycon4,
		   yycon5, dy1ty1, dy2ty1, dy3ty1, dy4ty1, dy5ty1,
		   zzcon1, zzcon2, zzcon3, zzcon4, zzcon5, dz1tz1,
		   dz2tz1, dz3tz1, dz4tz1, dz5tz1, dnxm1, dnym1,
		   dnzm1, c1c2, c1c5, c3c4, c1345, conz1, c1, c2,
		   c3, c4, c5, c4dssp, c5dssp, dtdssp, dttx1, bt,
		   dttx2, dtty1, dtty2, dttz1, dttz2, c2dttx1,
		   c2dtty1, c2dttz1, comz1, comz4, comz5, comz6,
		   c3c4tx3, c3c4ty3, c3c4tz3, c2iv, con43, con16;

	int maxcells, IMAX, JMAX, KMAX, MAX_CELL_DIM, BUF_SIZE, IMAXP, JMAXP;

	int predecessor[3], successor[3], grid_size[3];

	int west_size, east_size, bottom_size, top_size,
		north_size, south_size, start_send_west,
		start_send_east, start_send_south, start_send_north,
		start_send_bottom, start_send_top, start_recv_west,
		start_recv_east, start_recv_south, start_recv_north,
		start_recv_bottom, start_recv_top;

	logical timeron;

} fortran_vars_;

#define F fortran_vars_

// int
#define ncells				F.ncells
#define maxcells			F.maxcells
#define IMAX				F.IMAX
#define JMAX				F.JMAX
#define KMAX				F.KMAX
#define MAX_CELL_DIM		F.MAX_CELL_DIM
#define BUF_SIZE			F.BUF_SIZE
#define IMAXP				F.IMAXP
#define JMAXP				F.JMAXP
#define west_size			F.west_size
#define east_size			F.east_size
#define bottom_size			F.bottom_size
#define top_size			F.top_size
#define north_size			F.north_size
#define south_size			F.south_size
#define start_send_west		F.start_send_west
#define start_send_east		F.start_send_east
#define start_send_south	F.start_send_south
#define start_send_north	F.start_send_north
#define start_send_bottom	F.start_send_bottom
#define start_send_top		F.start_send_top
#define start_recv_west		F.start_recv_west
#define start_recv_east		F.start_recv_east
#define start_recv_south	F.start_recv_south
#define start_recv_north	F.start_recv_north
#define start_recv_bottom	F.start_recv_bottom
#define start_recv_top		F.start_recv_top

// double
#define tx1			F.tx1
#define tx2			F.tx2
#define tx3			F.tx3
#define ty1			F.ty1
#define ty2			F.ty2
#define ty3			F.ty3
#define tz1			F.tz1
#define tz2			F.tz2
#define tz3			F.tz3
#define dx1			F.dx1
#define dx2			F.dx2
#define dx3			F.dx3
#define dx4			F.dx4
#define dx5			F.dx5
#define dy1			F.dy1
#define dy2			F.dy2
#define dy3			F.dy3
#define dy4			F.dy4
#define dy5			F.dy5
#define dz1			F.dz1
#define dz2			F.dz2
#define dz3			F.dz3
#define dz4			F.dz4
#define dz5			F.dz5
#define dssp		F.dssp
#define dt			F.dt
#define dxmax		F.dxmax
#define dymax		F.dymax
#define dzmax		F.dzmax
#define xxcon1		F.xxcon1
#define xxcon2		F.xxcon2
#define xxcon3		F.xxcon3
#define xxcon4		F.xxcon4
#define xxcon5		F.xxcon5
#define dx1tx1		F.dx1tx1
#define dx2tx1		F.dx2tx1
#define dx3tx1		F.dx3tx1
#define dx4tx1		F.dx4tx1
#define dx5tx1		F.dx5tx1
#define yycon1		F.yycon1
#define yycon2		F.yycon2
#define yycon3		F.yycon3
#define yycon4		F.yycon4
#define yycon5		F.yycon5
#define dy1ty1		F.dy1ty1
#define dy2ty1		F.dy2ty1
#define dy3ty1		F.dy3ty1
#define dy4ty1		F.dy4ty1
#define dy5ty1		F.dy5ty1
#define zzcon1		F.zzcon1
#define zzcon2		F.zzcon2
#define zzcon3		F.zzcon3
#define zzcon4		F.zzcon4
#define zzcon5		F.zzcon5
#define dz1tz1		F.dz1tz1
#define dz2tz1		F.dz2tz1
#define dz3tz1		F.dz3tz1
#define dz4tz1		F.dz4tz1
#define dz5tz1		F.dz5tz1
#define dnxm1		F.dnxm1
#define dnym1		F.dnym1
#define dnzm1		F.dnzm1
#define c1c2		F.c1c2
#define c1c5		F.c1c5
#define c3c4		F.c3c4
#define c1345		F.c1345
#define conz1		F.conz1
#define c1			F.c1
#define c2			F.c2
#define c3			F.c3
#define c4			F.c4
#define c5			F.c5
#define c4dssp		F.c4dssp
#define c5dssp		F.c5dssp
#define dtdssp		F.dtdssp
#define dttx1		F.dttx1
#define bt			F.bt
#define dttx2		F.dttx2
#define dtty1		F.dtty1
#define dtty2		F.dtty2
#define dttz1		F.dttz1
#define dttz2		F.dttz2
#define c2dttx1		F.c2dttx1
#define c2dtty1		F.c2dtty1
#define c2dttz1		F.c2dttz1
#define comz1		F.comz1
#define comz4		F.comz4
#define comz5		F.comz5
#define comz6		F.comz6
#define c3c4tx3		F.c3c4tx3
#define c3c4ty3		F.c3c4ty3
#define c3c4tz3		F.c3c4tz3
#define c2iv		F.c2iv
#define con43		F.con43
#define con16		F.con16

// logical
#define timeron		F.timeron

// static arrays
#define GRID_POINTS(A)			F.grid_points[A - 1]
#define CE(A, B)				F.ce[A - 1][B - 1]
#define PREDECESSOR(A)			F.predecessor[A - 1]
#define SUCCESSOR(A)			F.successor[A - 1]
#define GRID_SIZE(A)			F.grid_size[A - 1]

/*
В файле mpinpb_c.h:
(int) node no_nodes total_nodes root comm_setup comm_solve comm_rhs dp_type
(logical) active
*/

#else // FORTRAN_INTERACTION

extern int ncells;
extern int grid_points[3];

extern double tx1, tx2, tx3, ty1, ty2, ty3, tz1, tz2, tz3,
			 dx1, dx2, dx3, dx4, dx5, dy1, dy2, dy3, dy4,
			 dy5, dz1, dz2, dz3, dz4, dz5, dssp, dt,
			 ce[13][5], dxmax, dymax, dzmax, xxcon1, xxcon2,
			 xxcon3, xxcon4, xxcon5, dx1tx1, dx2tx1, dx3tx1,
			 dx4tx1, dx5tx1, yycon1, yycon2, yycon3, yycon4,
			 yycon5, dy1ty1, dy2ty1, dy3ty1, dy4ty1, dy5ty1,
			 zzcon1, zzcon2, zzcon3, zzcon4, zzcon5, dz1tz1,
			 dz2tz1, dz3tz1, dz4tz1, dz5tz1, dnxm1, dnym1,
			 dnzm1, c1c2, c1c5, c3c4, c1345, conz1, c1, c2,
			 c3, c4, c5, c4dssp, c5dssp, dtdssp, dttx1, bt,
			 dttx2, dtty1, dtty2, dttz1, dttz2, c2dttx1,
			 c2dtty1, c2dttz1, comz1, comz4, comz5, comz6,
			 c3c4tx3, c3c4ty3, c3c4tz3, c2iv, con43, con16;

extern int maxcells, IMAX, JMAX, KMAX, MAX_CELL_DIM, BUF_SIZE, IMAXP, JMAXP;

extern int predecessor[3], successor[3], grid_size[3];

extern int west_size, east_size, bottom_size, top_size,
	north_size, south_size, start_send_west,
	start_send_east, start_send_south, start_send_north,
	start_send_bottom, start_send_top, start_recv_west,
	start_recv_east, start_recv_south, start_recv_north,
	start_recv_bottom, start_recv_top;

extern logical timeron;

// static arrays
#define GRID_POINTS(A)			grid_points[A - 1]
#define CE(A, B)				ce[A - 1][B - 1]
#define PREDECESSOR(A)			predecessor[A - 1]
#define SUCCESSOR(A)			successor[A - 1]
#define GRID_SIZE(A)			grid_size[A - 1]

#endif // FORTRAN_INTERACTION

//dynamic arrays

// int
extern darray cell_coord, cell_low, cell_high, cell_size, start, end, slice;

// double
extern darray u, us, vs, ws, qs, ainv, rho_i, speed, square, rhs, forcing, lhs;
extern darray in_buffer, out_buffer;
extern darray cv, rhon, rhos, rhoq, cuf, q, ue, buf;


#define IN_BUFFER(A)			in_buffer.d1[A-1]
#define OUT_BUFFER(A)			out_buffer.d1[A-1]
#define CV(A)					cv.d1[A+2]
#define RHON(A)					rhon.d1[A+2]
#define RHOS(A)					rhos.d1[A+2]
#define RHOQ(A)					rhoq.d1[A+2]
#define CUF(A)					cuf.d1[A+2]
#define Q(A)					q.d1[A+2]

#define CELL_COORD(A, B) 		cell_coord.i2[A-1][B-1]
#define CELL_LOW(A, B) 			cell_low.i2[A-1][B-1]
#define CELL_HIGH(A, B) 		cell_high.i2[A-1][B-1]
#define CELL_SIZE(A, B) 		cell_size.i2[A-1][B-1]
#define START(A, B) 			start.i2[A-1][B-1]
#define END(A, B) 				end.i2[A-1][B-1]
#define SLICE(A, B) 			slice.i2[A-1][B-1]
#define UE(A, B) 				ue.d2[A-1][B+2]
#define BUF(A, B) 				buf.d2[A-1][B+2]

#define US(A, B, C, D)			us.d4[A-1][B+1][C+1][D+1]
#define VS(A, B, C, D)			vs.d4[A-1][B+1][C+1][D+1]
#define WS(A, B, C, D)			ws.d4[A-1][B+1][C+1][D+1]
#define QS(A, B, C, D)			qs.d4[A-1][B+1][C+1][D+1]
#define AINV(A, B, C, D)		ainv.d4[A-1][B+1][C+1][D+1]
#define RHO_I(A, B, C, D)		rho_i.d4[A-1][B+1][C+1][D+1]
#define SPEED(A, B, C, D)		speed.d4[A-1][B+1][C+1][D+1]
#define SQUARE(A, B, C, D)		square.d4[A-1][B+1][C+1][D+1]

#define U(A, B, C, D, E)		u.d5[A-1][B-1][C+2][D+2][E+2]
#define RHS(A, B, C, D, E)		rhs.d5[A-1][B-1][C][D][E]
#define FORCING(A, B, C, D, E)	forcing.d5[A-1][B-1][C][D][E]
#define LHS(A, B, C, D, E)		lhs.d5[A-1][B-1][C][D][E]

#endif //SP_DATA_C_H

