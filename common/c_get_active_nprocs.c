#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <mpi.h>

typedef enum { false, true } logical;

////////////////////////////////////////////////////////////////////
// return the largest np1 and np2 such that np1 * np2 <= nprocs
// pkind = 1, np1 = np2                 (square number)
//         2, np1/2 <= np2 <= np1
//         3, np1 = np2 or np1 = np2*2  (power of 2)
// other outputs:
//     npa = np1 * np2 (active number of processes)
//     nprocs   - total number of processes
//     rank     - rank of this process
//     comm_out - MPI communicator
//     active   - true if this process is active; false otherwise
////////////////////////////////////////////////////////////////////

void get_active_nprocs(int pkind, int *p_np1, int *p_np2, int *p_npa,
					   int *p_nprocs, int *p_rank, MPI_Comm *comm_out, logical *p_active)
{
	int np1				= *p_np1;
	int np2				= *p_np2;
	int npa				= *p_npa;
	int nprocs			= *p_nprocs;
	int rank			= *p_rank;
	logical active 		= *p_active;

	MPI_Comm comm_in;
	int n, np1w, np2w, npaw;
	int ic, ios;
	char val[40];

	// nprocs and rank in COMM_WORLD
	comm_in = MPI_COMM_WORLD;
	MPI_Comm_size(comm_in, &nprocs);
	MPI_Comm_rank(comm_in, &rank);

	if (pkind <= 1)
	{
		// square number of processes (add small number to allow for roundoff)
		np2 = (int) (sqrt((double) nprocs) + 0.001);
		np1 = np2;
	}
	else
	{
		// power-of-two processes
		np1 = (int) ( log(((double) nprocs) + 0.001) / log(2.0) );
		np2 = np1 / 2;
		np1 = np1 - np2;
		np1 = 1 << np1;
		np2 = 1 << np2;
	}
	npa = np1 * np2;

	// for option 2, go further to get the best (np1 * np2) proc grid
	if (pkind == 2 && npa < nprocs)
	{
		np1w = (int) (sqrt( ((double) nprocs) + 0.001));
		np2w = (int) (sqrt( ((double) nprocs * 2) + 0.001));
		for (n = np1w; n <= np2w; n++)
		{
			npaw = nprocs / n * n;
			if (n == 1 && nprocs == 3)
				npaw = 2;
			if (npaw > npa)
			{
				npa = npaw;
				np1 = npa / n;
				if (np1 < n)
				{
					np2 = np1;
					np1 = n;
				}
				else
					np2 = n;
			}
		}
	}

	// all good if calculated is the same as requested
	*comm_out = comm_in;
	active = true;
	if (nprocs == npa)
		goto active_nprocs_end;

	// npa < nprocs, need to check if a strict NPROCS enforcement is required
	if (rank == 0)
	{
		//ios = getenv_s(&ic, val, 40, "NPB_NPROCS_STRICT");
		ic = snprintf(val, 40, "%s", getenv("NPB_NPROCS_STRICT"));

		if (ios == 0 && ic > 0)
			if (val[0] == '0' || val[0] == '-')
				active = false;
			else if (val[0] == 'o' && val[1] == 'f' && val[2] == 'f')
				active = false;
			else if (val[0] == 'O' && val[1] == 'F' && val[2] == 'F')
				active = false;
			else if (val[0] == 'n' || val[0] == 'N' || val[0] == 'f' || val[0] == 'F')
				active = false;
	}
	MPI_Bcast(&active, 1, MPI_LOGICAL, 0, comm_in);

	// abort if a strict NPROCS enforcement is required
	if (active)
	{
		if (rank == 0)
		{
			printf(" *** ERROR determining processor topology for %d processes\n", nprocs);

			printf("     Expecting a ");
			if (pkind <= 1)
				printf("square");
			else if (pkind == 2)
				printf("grid (nx*ny, nx/2<=ny<=nx)");
			else
				printf("power-of-two");

			printf(" number of processes (such as %d)\n", npa);
		}
		fflush(stdout);
		MPI_Abort(comm_in, MPI_ERR_OTHER);
		exit(1);
	}

	// mark excess ranks as inactive
	// split communicator based on rank value
	if (rank >= npa)
	{
		active = false;
		ic = 1;
	}
	else
	{
		active = true;
		ic = 0;
	}
	MPI_Comm_split(comm_in, ic, rank, comm_out);

active_nprocs_end:
	*p_np1		= np1;
	*p_np2		= np2;
	*p_npa		= npa;
	*p_nprocs	= nprocs;
	*p_rank		= rank;
	*p_active	= active;

	return;
}
