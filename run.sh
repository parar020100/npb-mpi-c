#!/bin/bash

unset no_run no_compile
if [[ $1 = "-c" || $1 = "--no-run" ]]
then
	no_run=1
	shift
fi
if [[ $1 = "-r" || $1 = "--no-compile" ]]
then
	no_compile=1
	shift
fi

benchmark=sp # ft mg cg lu bt is ep sp
result_dir=results/$(date +%Y-%m-%d_%H-%M-%S)

classes=(S W A B C D E F)
nums_of_threads=(1 4 9 16 25 36)
languages=(F M C)

################################################################################
line()
{
	symbol=$1
	length=$2
	line=""
	for ((i=0; i<$length; i++))
	do
		echo -n $symbol
	done
}

console_line_text()
{
	symbol=$1
	shift
	size=$2
	shift
	text=$@
	length=$(expr length "$text")
	width=$(tput cols)
	free_space=$((width - length - 2))
	half_line=$(line $symbol $((free_space / 2)))

	echo -n "$half_line $text $half_line"
	if [[ $((free_space % 2)) = 1 ]]
	then
		echo -n $symbol
	fi
}

console_line()
{
	symbol=$1
	shift
	width=$(tput cols)
	if [[ $# = 0 ]]
	then
		line $symbol $width
	else
		console_line_text $symbol $width $@
	fi
	echo
}

console_line_dashed() { console_line "╴" $@; }
console_line_normal() { console_line "―" $@; }
console_line_bold()   { console_line "━" $@; }
console_line_double() { console_line "═" $@; }

cl0() { console_line_dashed $@; }
cl1() { console_line_normal $@; }
cl2() { console_line_bold   $@; }
cl3() { console_line_double $@; }

################################################################################

mkdir -p bin

if [[ ! $no_compile ]]
then

	for class in ${classes[@]}
	do
		for lang in ${languages[@]}
		do
			cl1
			cl1 "[$(date +%H:%M:%S)] Compiling $benchmark.$class."
			echo
			make $benchmark CLASS=$class LANG=$lang -j
			[[ $? = 0 ]] || exit 1
			mv bin/$benchmark.$class.x bin/$benchmark.$class.$lang.x
		done
	done
	cl2
fi

if [[ ! $no_run ]]
then
	mkdir -p $result_dir

	for class in ${classes[@]}
	do
		for num_thread in ${nums_of_threads[@]}
		do
			for lang in ${languages[@]}
			do
				echo "[$(date +%H:%M:%S)] > Running $benchmark.$class. ($num_thread threads, $lang lang)"
				mpirun -np $num_thread bin/$lang/$benchmark.$class.x > $result_dir/$benchmark.$class.$num_thread.$lang.out
			done
		done
	done
	cl2
	cp -r ./bin $result_dir/bin
fi